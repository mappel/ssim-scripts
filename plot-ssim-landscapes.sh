#!/bin/bash
set -eou pipefail
DAT_FILE_NAME=ssim-landscape.dat
OUT_FILE_NAME=ssim-landscape.pdf
PLOT_SCRIPT=plot-ssim-landscape.gp

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    DAT_FILE="$VIDEO""$DAT_FILE_NAME"
    OUT_FILE="$VIDEO""$OUT_FILE_NAME"
    gnuplot -e 'IN_FILE="'"$DAT_FILE"'"' -e 'OUT_FILE="'"$OUT_FILE"'"' "$PLOT_SCRIPT"
done
