set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 0.5,0.25 nomirror norotate autojustify
set ytics border in scale 0.5,0.25 nomirror norotate autojustify

set style line 1 lc rgb "#b2e2e2"
set style line 2 lc rgb "#66c2a4"
set style line 3 lc rgb "#2ca25f"
set style line 4 lc rgb "#006d2c"

set xrange [0:76]
set xtics 10 offset 0,0.5
set mxtics 2
set xlabel "Segments" offset 0,1

set yrange[0:96]
set ylabel "Kept Frames" offset 1,0

set key autotitle columnhead
set key outside top center horizontal maxrows 1
set key samplen 1.5

set output OUT_FILE
plot \
    IN_FILE u 1:(96 - $2):(0) w filledcurves ls 4, \
    '' u 1:(96 - $3):(0) w filledcurves ls 3, \
    '' u 1:(96 - $4):(0) w filledcurves ls 2, \
    '' u 1:(96 - $5):(0) w filledcurves ls 1
unset output
