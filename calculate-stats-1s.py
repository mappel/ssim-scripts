#!/usr/bin/python3
import sys
import os
import numpy as np


SEGMENT_SSIM_FILE = 'ssim-frames.dat'
THRESHOLDS_FILE = 'thresholds-percent.dat'
EXPECTED_SEGMENT_COUNT = 300
SEGMENT_STAT_FILE = 'ssim-stats.dat'
THRESHOLDS_STAT_FILE = 'thresholds-stats.dat'


def get_segment_ssims(video_path: str):
    ret = list()
    it = os.scandir(video_path)
    for entry in it:
        if entry.is_dir():
            try:
                segment_no = int(entry.name)
            except ValueError as e:
                print('Skipping directory: ' + video_path + entry.name)
                continue
            ssim_file = video_path + entry.name + '/' + SEGMENT_SSIM_FILE
            if not os.path.exists(ssim_file):
                print('Could not find ' + SEGMENT_SSIM_FILE + ' in directory: ' + video_path + entry.name)
                continue
            ret.append((segment_no, ssim_file))
    if len(ret) != EXPECTED_SEGMENT_COUNT:
        print('Warning: Video directory does not meet expected segment count. Expected ' + str(EXPECTED_SEGMENT_COUNT)
                + ' got ' + str(len(ret)), file=sys.stderr)
        print('Directory: ' + video_path, file=sys.stderr)
    ret.sort()
    return ret


def get_segment_stats(ssim_file: str):
    scores = list()
    with open(ssim_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                print('Error: Invalid SSIM line: ' + line, file=sys.stderr)
                print('in file: ' + ssim_file, file=sys.stderr)
                return None
            try:
                ssim_score = float(line_split[4])
            except ValueError as e:
                print('Error: Invalid SSIM score: ' + str(e), file=sys.stderr)
                return None
            scores.append(ssim_score)
    score_array = np.array(scores)
    avg_score = score_array.mean()
    p90_score = np.percentile(score_array, 90)
    min_score = score_array.min()
    return avg_score, p90_score, min_score


def calculate_segment_stats(video_path: str):
    lines = ['segment_no avg p90 mn\n']
    ssim_files = get_segment_ssims(video_path)
    for segment_no, ssim_file in ssim_files:
        line = [segment_no]
        stats = get_segment_stats(ssim_file)
        if stats is None:
            return
        line += stats
        lines.append(' '.join(map(str, line)) + '\n')
    out_file = video_path + SEGMENT_STAT_FILE
    with open(out_file, 'w') as f:
        f.writelines(lines)


def calculate_threshold_stats(video_path: str):
    threshold_location = video_path + THRESHOLDS_FILE
    if not os.path.exists(threshold_location):
        print('Error: Failed to find thresholds file: ' + threshold_location, file=sys.stderr)
        return
    with open(video_path + THRESHOLDS_FILE, 'r') as f:
        headers = f.readline()
        threshold_count = len(headers.split()) - 1
        if threshold_count < 1:
            print('Error: No thresholds found in file: ' + threshold_location, file=sys.stderr)
            return
        thresholds = headers.split()[1:]
        threshold_saves = []
        for i in range(0, threshold_count):
            threshold_saves.append([])
        for line in f:
            line_split = line.split()
            if len(line_split) != threshold_count + 1:
                print('Error: Invalid threshold line: ' + line, file=sys.stderr)
                return
            for i in range(1, len(line_split)):
                try:
                    percent_discarded = float(line_split[i])
                except ValueError as e:
                    print('Error: Invalid amount: ' + str(e), file=sys.stderr)
                    return
                if percent_discarded < 0 or percent_discarded > 100:
                    print('Error: Invalid amount: ' + str(percent_discarded), file=sys.stderr)
                    return
                threshold_saves[i-1].append(percent_discarded)
    saves_arrays = list()
    for save in threshold_saves:
        saves_arrays.append(np.array(save))
    lines = ['metric ' + ' '.join(thresholds) + '\n']
    line = ['mn']
    for save_array in saves_arrays:
        line.append(str(save_array.min()))
    lines.append(' '.join(line) + '\n')
    line = ['p10']
    for save_array in saves_arrays:
        line.append(str(np.percentile(save_array, 10)))
    lines.append(' '.join(line) + '\n')
    line = ['p50']
    for save_array in saves_arrays:
        line.append(str(np.percentile(save_array, 50)))
    lines.append(' '.join(line) + '\n')
    line = ['avg']
    for save_array in saves_arrays:
        line.append(str(save_array.mean()))
    lines.append(' '.join(line) + '\n')
    line = ['mx']
    for save_array in saves_arrays:
        line.append(str(save_array.max()))
    lines.append(' '.join(line) + '\n')
    out_file = video_path + THRESHOLDS_STAT_FILE
    with open(out_file, 'w') as f:
        f.writelines(lines)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/video>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    print(video_path)
    calculate_segment_stats(video_path)
    calculate_threshold_stats(video_path)
