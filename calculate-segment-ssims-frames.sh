#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}
readonly LOG_DIR
readonly EXPECTED_FRAME_COUNT=96

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    for SEGMENT in "$VIDEO"*/
    do
        ./calculate-segment-ssim-frames.py "$SEGMENT" "$EXPECTED_FRAME_COUNT"
    done
done
