#!/usr/bin/python3
import argparse
import os
import sys


VIDEO_DIR_SUFFIX = '_dash'
RANGE_FILE_SUFFIX = '.range'
SIZES_FILE = 'acc-frame-sizes.dat'
FRAME_SIZE_DELIMITER = ' '


def sanitize_path(path: str) -> str:
    if not path.endswith('/'):
        path += '/'
    return path


def check_unmatched_dirs(proc_dirs: set, log_dirs: set) -> None:
    unmatched_dirs = proc_dirs.symmetric_difference(log_dirs)
    if unmatched_dirs:
        print(f'Warning: Failed to find matching log or proc directory for: '
              f'{unmatched_dirs}')


def write_output(output_file: str, accumulated_frame_sizes: list) -> None:
    lines = [' '.join(map(str, (idx, size))) 
             for idx, size in enumerate(accumulated_frame_sizes)]
    with open(output_file, 'w') as f:
        f.writelines('\n'.join(lines) + '\n')


def get_video_directories(path: str) -> set:
    ret = set()
    with os.scandir(path) as it:
        for entry in it:
            if not entry.is_dir() or not entry.name.endswith(VIDEO_DIR_SUFFIX):
                continue
            ret.add(entry.name)
    return ret


def get_segment_directories(path: str) -> set:
    ret = set()
    with os.scandir(path) as it:
        for entry in it:
            if not entry.is_dir() or not entry.name.isdigit():
                continue
            ret.add(entry.name)
    return ret


def get_max_range_file(path: str) -> str:
    max_dropped_frames = -1
    max_range_file = str()
    with os.scandir(path) as it:
        for entry in it:
            if not entry.is_file() \
                or not entry.name.endswith(RANGE_FILE_SUFFIX):
                continue
            range_file = f'{path}{entry.name}'
            dropped_frames_str = entry.name[:-len(RANGE_FILE_SUFFIX)]
            if not dropped_frames_str.isdigit():
                print(f'Warning: Ignoring range file due to malformed name: '
                      f'{range_file}')
                continue
            dropped_frames = int(dropped_frames_str)
            if dropped_frames > max_dropped_frames:
                max_dropped_frames = dropped_frames
                max_range_file = range_file
    return max_range_file


def get_accumulated_frame_sizes(range_file: str) -> list:
    frame_sizes = list()
    with open(range_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                raise ValueError(f'Invalid range ({line.strip()}) in range '
                                 f'file: {range_file}')
            start, end = line_split
            if not start.isdigit() or not end.isdigit():
                raise ValueError(f'Invalid range ({line.strip()}) in range '
                                 f'file: {range_file}')
            frame_sizes.append(int(end) - int(start) + 1)
    # The format of the output file is
    #   number_of_dropped_frames remaining_size_in_bytes
    # i.e., the first line starts with zero dropped frames and the total
    # number of bytes that can be dropped.
    # The last line contains the total number of frames that are dropped and a
    # remaining size of zero.
    #
    # The range file contains the frame ranges in the _reverse_ order in which
    # they are dropped, i.e., the last line is the _first_ frame that is
    # dropped.
    # Therefore, we reverse the list first, calculate the total size of all
    # frames, and then subtract the accumulated size of the dropped frames
    # from the total size.
    frame_sizes.reverse()
    total_frame_size = sum(frame_sizes)
    accumulated_frame_sizes = [total_frame_size]
    for frame_size in frame_sizes:
        accumulated_frame_sizes.append(accumulated_frame_sizes[-1] - frame_size)

    return accumulated_frame_sizes


def process_video(proc_path: str, log_path: str, video: str):
    print(f'Processing video: {video}')
    video_proc_dir = f'{proc_path}{video}/'
    video_log_dir = f'{log_path}{video}/'

    proc_segment_dirs = get_segment_directories(video_proc_dir)
    log_segment_dirs = get_segment_directories(video_log_dir)
    segment_dirs = proc_segment_dirs.intersection(log_segment_dirs)
    print(f'Segments: {len(segment_dirs)}')
    check_unmatched_dirs(proc_segment_dirs, log_segment_dirs)

    for segment in segment_dirs:
        segment_proc_dir = f'{video_proc_dir}{segment}/'
        output_file = f'{video_log_dir}{segment}/{SIZES_FILE}'
        range_file = get_max_range_file(segment_proc_dir)
        accumulated_frame_sizes = get_accumulated_frame_sizes(range_file)
        write_output(output_file, accumulated_frame_sizes)


def main() -> None:
    desc = """Calculate the accumulated size of dropped frames by inspecting
    the range files that are used during the SSIM calculation. Requires the
    same directory structure for the proc and logs paths."""
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('proc_path')
    parser.add_argument('log_path')
    args = parser.parse_args()

    proc_path = sanitize_path(args.proc_path)
    log_path = sanitize_path(args.log_path)

    proc_video_dirs = get_video_directories(proc_path)
    log_video_dirs = get_video_directories(log_path)
    video_dirs = proc_video_dirs.intersection(log_video_dirs)
    if not video_dirs:
        print('No matching proc/log directories found.')
        sys.exit(0)
    check_unmatched_dirs(proc_video_dirs, log_video_dirs)

    for video in video_dirs:
        process_video(proc_path, log_path, video)


if __name__ == '__main__':
    main()
    sys.exit(0)
