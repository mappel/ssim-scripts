#!/bin/bash
BITKILLER=/pool/data/mappel/bitkiller/build/bitkiller
FFMPEG=/pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg

if [ ! $# -eq 6 ]
then
    echo "usage: $0 <kill-input> <ref-input> <init-segment> <kill-range> <log-output> <path/to/scratch>"
    exit
fi

KILL_INPUT="$1"
REF_INPUT="$2"
INIT_SEGMENT="$3"
KILL_RANGE="$4"
LOG_OUTPUT="$5"
SCRATCH_DIR=${6%/}

CORRUPT_BASE=$(mktemp -p $SCRATCH_DIR --suffix=.mp4)
CORRUPT_GLUED=$(mktemp -p $SCRATCH_DIR --suffix=.mp4)

LOG_PATH=$(dirname "$LOG_OUTPUT")

"$BITKILLER" "$KILL_RANGE" "$KILL_INPUT" "$CORRUPT_BASE" -q
cat "$INIT_SEGMENT" "$CORRUPT_BASE" > "$CORRUPT_GLUED"
mkdir -p "$LOG_PATH"
"$FFMPEG" -i "$CORRUPT_GLUED" -i "$REF_INPUT" -lavfi "[0]scale=3840x2160[c];[c][1]libvmaf=model_path=/pool/data/mappel/vmaf-1.3.15/model/vmaf_4k_v0.6.1.pkl:log_path=$LOG_OUTPUT:log_fmt=json:pool=mean:n_threads=8:psnr=1" -f null - 2>> /dev/null
rm "$CORRUPT_BASE" "$CORRUPT_GLUED"
