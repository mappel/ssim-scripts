#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    ./calculate-ssim-thresholds-frames.py "$VIDEO"
done
