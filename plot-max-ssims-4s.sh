#!/bin/bash
set -eou pipefail
DAT_FILE_NAME=max-ssims.dat
OUT_FILE_SUFFIX="max-ssims.pdf"
PLOT_SCRIPT=plot-max-ssims.gp

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}
# BBB
gnuplot \
        -e "IN_FILE01='$LOG_DIR/bbb_144p_0k16_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE02='$LOG_DIR/bbb_240p_0k235_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE03='$LOG_DIR/bbb_240p_0k375_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE04='$LOG_DIR/bbb_360p_0k56_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE05='$LOG_DIR/bbb_360p_0k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE06='$LOG_DIR/bbb_480p_1k05_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE07='$LOG_DIR/bbb_480p_1k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE08='$LOG_DIR/bbb_720p_2k35_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE09='$LOG_DIR/bbb_720p_3k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE10='$LOG_DIR/bbb_1080p_4k3_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE11='$LOG_DIR/bbb_1080p_5k8_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE12='$LOG_DIR/bbb_1440p_7k4_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE13='$LOG_DIR/bbb_2160p_10k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "OUT_FILE='$LOG_DIR/bbb-$OUT_FILE_SUFFIX'" \
        $PLOT_SCRIPT
# ED
gnuplot \
        -e "IN_FILE01='$LOG_DIR/ed_144p_0k16_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE02='$LOG_DIR/ed_240p_0k235_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE03='$LOG_DIR/ed_240p_0k375_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE04='$LOG_DIR/ed_360p_0k56_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE05='$LOG_DIR/ed_360p_0k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE06='$LOG_DIR/ed_480p_1k05_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE07='$LOG_DIR/ed_480p_1k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE08='$LOG_DIR/ed_720p_2k35_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE09='$LOG_DIR/ed_720p_3k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE10='$LOG_DIR/ed_1080p_4k3_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE11='$LOG_DIR/ed_1080p_5k8_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE12='$LOG_DIR/ed_1080p_7k4_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE13='$LOG_DIR/ed_1080p_10k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "OUT_FILE='$LOG_DIR/ed-$OUT_FILE_SUFFIX'" \
        $PLOT_SCRIPT
# SINTEL
gnuplot \
        -e "IN_FILE01='$LOG_DIR/sintel_144p_0k16_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE02='$LOG_DIR/sintel_240p_0k235_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE03='$LOG_DIR/sintel_240p_0k375_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE04='$LOG_DIR/sintel_360p_0k56_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE05='$LOG_DIR/sintel_360p_0k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE06='$LOG_DIR/sintel_480p_1k05_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE07='$LOG_DIR/sintel_480p_1k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE08='$LOG_DIR/sintel_720p_2k35_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE09='$LOG_DIR/sintel_720p_3k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE10='$LOG_DIR/sintel_1080p_4k3_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE11='$LOG_DIR/sintel_1080p_5k8_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE12='$LOG_DIR/sintel_1440p_7k4_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE13='$LOG_DIR/sintel_2160p_10k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "OUT_FILE='$LOG_DIR/sintel-$OUT_FILE_SUFFIX'" \
        $PLOT_SCRIPT
# TOS
gnuplot \
        -e "IN_FILE01='$LOG_DIR/tos_144p_0k16_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE02='$LOG_DIR/tos_240p_0k235_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE03='$LOG_DIR/tos_240p_0k375_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE04='$LOG_DIR/tos_360p_0k56_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE05='$LOG_DIR/tos_360p_0k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE06='$LOG_DIR/tos_480p_1k05_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE07='$LOG_DIR/tos_480p_1k75_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE08='$LOG_DIR/tos_720p_2k35_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE09='$LOG_DIR/tos_720p_3k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE10='$LOG_DIR/tos_1080p_4k3_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE11='$LOG_DIR/tos_1080p_5k8_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE12='$LOG_DIR/tos_1440p_7k4_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "IN_FILE13='$LOG_DIR/tos_2160p_10k_24fps_96key_300s_dash/$DAT_FILE_NAME'" \
        -e "OUT_FILE='$LOG_DIR/tos-$OUT_FILE_SUFFIX'" \
        $PLOT_SCRIPT
