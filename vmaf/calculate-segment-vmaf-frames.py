#!/usr/bin/python3
import sys
import os


OUTFILE_NAME = 'vmaf-frames.dat'
LOG_FILE_SUFFIX = '.log'
EXPECTED_FRAME_COUNT = 96


def get_logfiles(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_FILE_SUFFIX):
            log_no = int(entry.name[:-len(LOG_FILE_SUFFIX)])
            ret.append((log_no, path + entry.name))
    ret.sort()
    return ret


def parse_line(line: str):
    line_split = line.split(':')
    if len(line_split) != 2:
        raise ValueError('Invalid VMAF line: ' + line)
    key = line_split[0].strip('"')
    value = line_split[1].rstrip(',')
    return key, value


def parse_logfile(file: str):
    score = None
    highest_frame_found = -1
    with open(file, 'r') as f:
        for line in f:
            if 'frameNum' in line or 'VMAF score' in line:
                key, value = parse_line(line.strip())
                if key == 'frameNum':
                    highest_frame_found = int(value)
                elif key == 'VMAF score':
                    score = value
                else:
                    raise ValueError('"frameNum" or "VMAF score" in line, but key was different: ' + line.strip() + '\n in file: ' + file)
    # VMAF logs start counting frames at 0.
    if highest_frame_found +1 != EXPECTED_FRAME_COUNT:
        print('Warning: Log file does not meet expected frame count. Expected ' + str(EXPECTED_FRAME_COUNT) + ' got '
                + str(highest_frame_found + 1), file=sys.stderr)
        print('in file: ' + file, file=sys.stderr)
    if score is None:
        raise ValueError('Failed to find VMAF score in file: ' + file)
    return score


def write_vmaf_file(path: str, scores: list):
    with open(path + OUTFILE_NAME, 'w') as f:
        for log_no, score in scores:
            line = [str(log_no), str(score)]
            f.write(' '.join(line) + '\n')


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/segment>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    log_files = get_logfiles(log_path)
    if len(log_files) == 0:
        print('Error: No log files found in path: ' + log_path, file=sys.stderr)
        exit(1)
    scores = list()
    for log_no, log_file in log_files:
        score = parse_logfile(log_file)
        scores.append((log_no, score))
    write_vmaf_file(log_path, scores)
    exit(0)
