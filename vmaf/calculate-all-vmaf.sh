#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

./calculate-segment-vmaf-frames.sh "$LOG_DIR"
./translate-segment-vmaf-to-percent.sh "$LOG_DIR"
