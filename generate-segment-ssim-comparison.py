#!/usr/bin/python3
import sys
import os

SSIM_FILE = 'ssim-percent.dat'

def get_segment_ssim_files(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            try:
                segment_no = int(entry.name)
            except ValueError as e:
                print('Ignoring directory: ' + entry.name)
                continue
            segment_ssim_file = path + entry.name + '/' + SSIM_FILE
            if os.path.exists(segment_ssim_file):
                ret.append((segment_no, segment_ssim_file))
            else:
                print('Could not find SSIM file in directory: ' + path + entry.name)
    ret.sort()
    return ret


def get_scores_from_file(file: str):
    ret = list()
    with open(file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                raise ValueError('Invalid SSIM line: ' + line + '\n in file: ' + file)
            ret.append((line_split[0], line_split[4]))
    return ret


def generate_segment_comparison(segment_no: int, ssim_file_1: str, ssim_file_2: str, output_path: str):
    video_1_scores = get_scores_from_file(ssim_file_1)
    video_2_scores = get_scores_from_file(ssim_file_2)
    if len(video_1_scores) != len(video_2_scores):
        raise ValueError('Number of SSIM entries does not match. Video 1: ' + str(len(video_1_scores)) + ' Video 2: '
                + str(len(video_2_scores)) + ' Segment: ' + str(segment_no))
    with open(output_path + str(segment_no) + '.dat', 'w') as f:
        for idx, (video_1_percent, video_1_score) in enumerate(video_1_scores):
            video_2_percent, video_2_score = video_2_scores[idx]
            line = [video_1_percent]
            line.append(video_1_score)
            line.append(video_2_percent)
            line.append(video_2_score)
            f.write(' '.join(line) + '\n')


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print('usage: ' + sys.argv[0] + ' <path/to/video1> <path/to/video2> <path/to/output>')
        exit(1)
    log_path_1 = sys.argv[1]
    if not log_path_1.endswith('/'):
        log_path_1 += '/'
    log_path_2 = sys.argv[2]
    if not log_path_2.endswith('/'):
        log_path_2 += '/'
    output_path  = sys.argv[3]
    if not output_path.endswith('/'):
        output_path += '/'
    video_1_ssim_files = get_segment_ssim_files(log_path_1)
    video_2_ssim_files = get_segment_ssim_files(log_path_2)
    if len(video_1_ssim_files) != len(video_2_ssim_files):
        print('Error: Number of segment SSIM files does not match. Video 1: ' + str(len(video_1_ssim_files))
                + ' Video 2: ' + str(len(video_2_ssim_files)), file=sys.stderr)
        exit(1)
    for idx, (segment_no, segment_ssim_file) in enumerate(video_1_ssim_files):
        generate_segment_comparison(segment_no, segment_ssim_file, video_2_ssim_files[idx][1], output_path)

