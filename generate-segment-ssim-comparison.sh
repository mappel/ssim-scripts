#!/bin/bash
if [ ! $# -eq 3 ]
then
    echo "usage: $0 <path/to/logs1> <path/to/logs2> <path/to/output>"
    exit
fi
LOG_PATH_1=${1%/}
LOG_PATH_2=${2%/}
OUTPUT_PATH=${3%/}

for VIDEO in "$LOG_PATH_1"/*/
do
    VIDEO_NAME=$(basename $VIDEO)
    echo "$VIDEO_NAME"
    if [ ! -d "$LOG_PATH_2"/"$VIDEO_NAME" ]
    then
        echo "Error: No match for $VIDEO_NAME found in path $LOG_PATH_2"
        continue
    fi
    INPUT_VIDEO_1="$LOG_PATH_1"/"$VIDEO_NAME"
    INPUT_VIDEO_2="$LOG_PATH_2"/"$VIDEO_NAME"
    OUTPUT_VIDEO_PATH="$OUTPUT_PATH"/"$VIDEO_NAME"
    mkdir -p "$OUTPUT_VIDEO_PATH"
    ./generate-segment-ssim-comparison.py "$INPUT_VIDEO_1" "$INPUT_VIDEO_2" "$OUTPUT_VIDEO_PATH"
done
