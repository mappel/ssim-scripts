set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid size 14,140
# set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid size 14,35
set output OUT_FILE
set multiplot layout 60,5 margins screen .05, .95, .08, .93 spacing screen .07,.12
# set multiplot layout 15,5 margins screen .05, .95, .08, .93 spacing screen .07,.12

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, y2tics ls 81, ls 81, ls 82, ls 81

set xtics border in scale 0.5,0.25 nomirror norotate autojustify
set ytics border in scale 0.5,0.25 nomirror norotate autojustify

set style line 1 lc rgb "#2a71b0"

set xrange [0:24]
set xtics 20 offset 0,0.5
set mxtics 2

set ylabel "SSIM score" offset 1,0

unset key
unset ylabel
do for [i=1:SEGMENTS] {
IN_FILE=sprintf('%s/%d/%s',PREFIX, i, DAT_FILE)
stats IN_FILE u 1:5 nooutput
set yrange[STATS_min_y - 0.1:1]
plot IN_FILE u 1:5 w steps ls 1, \
    0.5 w l lc "black", \
    0.88 w l lc "black", \
    0.95 w l lc "black", \
    0.99 w l lc "black"
set yrange[0:1]
}
unset multiplot
unset output
