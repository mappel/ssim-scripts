for loss in $(seq 0.005 0.005 0.025)
do
    mkdir -p /dev/shm/ssim/ref-lossy/"$loss"
    ./generate-lossy-ref.py /dev/shm/ssim/ref /dev/shm/ssim/ref-lossy/"$loss"/bbb/ /pool/data/mappel/videos/bbb/slipstream-original-bbb.mpd "$loss"
done
