set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 0.5,0.25 nomirror norotate autojustify
set ytics border in scale 0.5,0.25 nomirror norotate autojustify

set style line 1 lc rgb "#2a71b0" lw 0.5
set style line 2 lc rgb "#e32322" lw 0.5
set style line 3 lc rgb "#f4e500" lw 0.5
set style line 4 lc rgb "#444e99" lw 0.5
set style line 5 lc rgb "#ea621f" lw 0.5
set style line 6 lc rgb "#8cbb26" lw 0.5
set style line 7 lc rgb "#6d398b" lw 0.5
set style line 8 lc rgb "#f18e1c" lw 0.5
set style line 9 lc rgb "#008e5b" lw 0.5
set style line 10 lc rgb "#c4037d" lw 0.5
set style line 11 lc rgb "#fdc60b" lw 0.5
set style line 12 lc rgb "#0696bb" lw 0.5
set style line 13 lc rgb "#e3228f" lw 0.5

set xrange [1:75]
set xtics 10 offset 0,0.5
set mxtics 2
set xlabel "Segments" offset 0,1

set yrange[0.75:1]
set ylabel "SSIM score" offset 1,0

set key outside top horizontal maxrows 3 font ',20'
set key samplen 2

set output OUT_FILE
stats IN_FILE01 u 1:2 nooutput
plot \
     IN_FILE13 u ($1):($2):(0) t '2160p' w lines ls 1,\
     IN_FILE12 u ($1):($2):(0) t '1440p' w lines ls 2,\
     IN_FILE10 u ($1):($2):(0) t '1080p' w lines  ls 4,\
     IN_FILE09 u ($1):($2):(0) t '720p' w lines  ls 5,\
     IN_FILE07 u ($1):($2):(0) t '480p' w lines ls 7 ,\
     IN_FILE05 u ($1):($2):(0) t '360p' w lines ls 9
unset output
