#!/usr/bin/python3
import sys
import os


SSIM_DAT_FILE = 'ssim-frames.dat'
OUT_DAT_FILE = 'thresholds-frames.dat'
THRESHOLDS = [0.99, 0.95, 0.88, 0.5]
USE_RIGHTMOST_IDX = False


def get_segment_log_files(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            expected_file = path + entry.name + '/' + SSIM_DAT_FILE
            if not os.path.exists(expected_file):
                print('Warning: Skipping directory without dat file: ' + path + entry.name, file=sys.stderr)
                continue
            segment_no = int(entry.name)
            ret.append((segment_no, expected_file))
    ret.sort()
    expected_segment_no = 1
    for segment_no, file in ret:
        if segment_no != expected_segment_no:
            raise ValueError('Gap in segment count. Got ' + str(segment_no) + ' expected ' + str(expected_segment_no))
        expected_segment_no += 1
    return ret


def get_scores_from_file(log_file: str):
    ret = list()
    with open(log_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                raise ValueError('Invalid SSIM line: ' + line + '\n in file: ' + log_file)
            all_score = float(line_split[4])
            ret.append(all_score)
    return ret

def get_thresholds_for_segment(segment_no: int, log_file: str):
    scores = get_scores_from_file(log_file)
    max_frame_threshold = len(scores) - 1
    frame_thresholds = [-1] * len(THRESHOLDS)
    for idx, score in enumerate(scores):
        for t_idx, threshold in enumerate(THRESHOLDS):
            if frame_thresholds[t_idx] == -1 and score < threshold:
                frame_thresholds[t_idx] = max(idx - 1, 0)
            elif frame_thresholds[t_idx] != -1 and score >= threshold:
                print('Segment ' + str(segment_no) + ': Warning: Threshold ' + str(threshold) + ' was set at idx ' + str(frame_thresholds[t_idx])
                        + ' but score increased to ' + str(score) + ' at idx ' + str(idx), file=sys.stderr)
                if USE_RIGHTMOST_IDX:
                    frame_thresholds[t_idx] = -1
    for idx, threshold in enumerate(frame_thresholds):
        if threshold == -1:
            print('Segment ' + str(segment_no) + ': Threshold ' + str(THRESHOLDS[idx]) + ' is never reached. Setting to ' + str(max_frame_threshold))
            frame_thresholds[idx] = max_frame_threshold
    return frame_thresholds


def write_frame_thresholds(path: str, frame_thresholds: list):
    segment_counter = 1
    with open(video_path + OUT_DAT_FILE, 'w') as f:
        f.write('segment_no ' + ' '.join(map(str, THRESHOLDS)) + '\n')
        for threshold in frame_thresholds:
            f.write(str(segment_counter) + ' ' + ' '.join(map(str, threshold)) + '\n')
            segment_counter += 1


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/video>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    log_files = get_segment_log_files(video_path)
    if len(log_files) == 0:
        raise ValueError('No (valid) log files found in path: ' + video_path)
    frame_thresholds = list()
    for segment_no, log_file in log_files:
        frame_thresholds.append(get_thresholds_for_segment(segment_no, log_file))
    write_frame_thresholds(video_path, frame_thresholds)

