#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

./plot-segment-ssims-frames.sh "$LOG_DIR"
./plot-segment-ssims-frames-all.sh "$LOG_DIR"
./plot-segment-ssims-percent.sh "$LOG_DIR"
./plot-segment-ssims-percent-all.sh "$LOG_DIR"
./plot-ssim-thresholds-frames.sh "$LOG_DIR"
./plot-ssim-thresholds-percent.sh "$LOG_DIR"
./plot-ssim-landscapes.sh "$LOG_DIR"
