#!/usr/bin/python3
import argparse
import sys
import os


SEGMENT_SSIM_FILE = 'ssim-frames.dat'
MAX_SSIMS_FILE = 'max-ssims.dat'


def get_segment_ssims(video_path: str, expected_segment_count: int):
    ret = list()
    it = os.scandir(video_path)
    for entry in it:
        if entry.is_dir():
            try:
                segment_no = int(entry.name)
            except ValueError as e:
                print('Skipping directory: ' + video_path + entry.name)
                continue
            ssim_file = video_path + entry.name + '/' + SEGMENT_SSIM_FILE
            if not os.path.exists(ssim_file):
                print('Could not find ' + SEGMENT_SSIM_FILE + ' in directory: ' + video_path + entry.name)
                continue
            ret.append((segment_no, ssim_file))
    if len(ret) != expected_segment_count:
        print('Warning: Video directory does not meet expected segment count. Expected ' + str(expected_segment_count)
                + ' got ' + str(len(ret)), file=sys.stderr)
        print('Directory: ' + video_path, file=sys.stderr)
    ret.sort()
    return ret


def get_segment_max_ssim(ssim_file: str):
    score = None
    with open(ssim_file, 'r') as f:
        line = f.readline()
        line_split = line.split()
        if len(line_split) != 5:
            print('Error: Invalid SSIM line: ' + line, file=sys.stderr)
            print('in file: ' + ssim_file, file=sys.stderr)
            return None
        try:
            ssim_score = float(line_split[4])
        except ValueError as e:
            print('Error: Invalid SSIM score: ' + str(e), file=sys.stderr)
            return None
        score = ssim_score
    return score


def get_max_ssims(video_path: str, expected_segment_count: int):
    lines = ['segment_no ssim\n']
    ssim_files = get_segment_ssims(video_path, expected_segment_count)
    for segment_no, ssim_file in ssim_files:
        line = [segment_no]
        score = get_segment_max_ssim(ssim_file)
        if score is None:
            return
        line.append(score)
        lines.append(' '.join(map(str, line)) + '\n')
    out_file = video_path + MAX_SSIMS_FILE
    with open(out_file, 'w') as f:
        f.writelines(lines)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('video_path')
    parser.add_argument('expected_segment_count', type=int)
    args = parser.parse_args()

    video_path = args.video_path
    if not video_path.endswith('/'):
        video_path += '/'
    print(video_path)
    get_max_ssims(video_path, args.expected_segment_count)


if __name__ == "__main__":
    main()
    sys.exit(0)
