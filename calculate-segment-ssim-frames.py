#!/usr/bin/python3
import argparse
import sys
import os


OUTFILE_NAME = 'ssim-frames.dat'
LOG_FILE_SUFFIX = '.log'


def get_logfiles(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_FILE_SUFFIX):
            log_no_str = entry.name[:-len(LOG_FILE_SUFFIX)]
            if not log_no_str.isdigit():
                continue
            log_no = int(entry.name[:-len(LOG_FILE_SUFFIX)])
            ret.append((log_no, path + entry.name))
    ret.sort()
    return ret


def parse_line(line: str):
    line_split = line.split()
    if len(line_split) != 6:
        raise ValueError('Invalid SSIM line: ' + line)
    y = float(line_split[1].lstrip('Y:'))
    u = float(line_split[2].lstrip('U:'))
    v = float(line_split[3].lstrip('V:'))
    all = float(line_split[4].lstrip('All:'))
    return y,u,v,all


def parse_logfile(file: str, expected_frame_count: int):
    y_acc = 0
    u_acc = 0
    v_acc = 0
    all_acc = 0
    line_count = 0
    with open(file, 'r') as f:
        for line in f:
            y,u,v,all = parse_line(line)
            y_acc += y
            u_acc += u
            v_acc += v
            all_acc += all
            line_count += 1
    if line_count == 0:
        raise ValueError('Empty log file: ' + file)
    if line_count != expected_frame_count:
        print('Warning: Log file does not meet expected frame count. Expected ' + str(expected_frame_count) + ' got '
                + str(line_count), file=sys.stderr)
        print('in file: ' + file, file=sys.stderr)
    y_avg = y_acc / line_count
    u_avg = u_acc / line_count
    v_avg = v_acc / line_count
    all_avg = all_acc / line_count
    return y_avg, u_avg, v_avg, all_avg


def write_ssim_file(path: str, scores: list):
    with open(path + OUTFILE_NAME, 'w') as f:
        for log_no, y, u, v, all in scores:
            line = [str(log_no)]
            line.append(str(y))
            line.append(str(u))
            line.append(str(v))
            line.append(str(all))
            f.write(' '.join(line) + '\n')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('segment_path')
    parser.add_argument('expected_frame_count', type=int)
    args = parser.parse_args()

    log_path = args.segment_path
    if not log_path.endswith('/'):
        log_path += '/'
    log_files = get_logfiles(log_path)
    if len(log_files) == 0:
        print('Error: No log files found in path: ' + log_path, file=sys.stderr)
        exit(1)
    scores = list()
    for log_no, log_file in log_files:
        y,u,v,all = parse_logfile(log_file, args.expected_frame_count)
        scores.append((log_no, y, u, v, all))
    write_ssim_file(log_path, scores)


if __name__ == "__main__":
    main()
    sys.exit(0)
