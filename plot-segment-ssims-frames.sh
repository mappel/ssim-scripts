#!/bin/bash
set -eou pipefail
DAT_FILE_NAME=ssim-frames.dat
OUT_FILE_NAME=ssim-frames.pdf
PLOT_SCRIPT=plot-segment-ssim-frames.gp

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    for SEGMENT in "$VIDEO"*/
    do
        DAT_FILE="$SEGMENT""$DAT_FILE_NAME"
        OUT_FILE="$SEGMENT""$OUT_FILE_NAME"
        gnuplot -e 'IN_FILE="'"$DAT_FILE"'"' -e 'OUT_FILE="'"$OUT_FILE"'"' "$PLOT_SCRIPT"
    done
done
