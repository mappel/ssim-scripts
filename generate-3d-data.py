#!/usr/bin/python3
import sys
import os


SSIM_DAT_FILE = 'ssim-frames.dat'
OUT_DAT_FILE = 'ssim-landscape.dat'


def get_segment_log_files(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            expected_file = path + entry.name + '/' + SSIM_DAT_FILE
            if not os.path.exists(expected_file):
                print('Warning: Skipping directory without dat file: ' + path + entry.name, file=sys.stderr)
                continue
            segment_no = int(entry.name)
            ret.append((segment_no, expected_file))
    ret.sort()
    expected_segment_no = 1
    for segment_no, file in ret:
        if segment_no != expected_segment_no:
            raise ValueError('Gap in segment count. Got ' + str(segment_no) + ' expected ' + str(expected_segment_no))
        expected_segment_no += 1
    return ret


def generate_segment_lines(segment_no: int, log_file: str):
    ret = list()
    with open(log_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                raise ValueError('Invalid SSIM line: ' + line + '\n in file: ' + log_file)
            dropped_frame_count = line_split[0]
            all_score = line_split[4]
            ret.append(str(segment_no) + ' ' + all_score + ' ' + dropped_frame_count + '\n')
    ret.append('\n')
    return ret


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/video>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    log_files = get_segment_log_files(video_path)
    if len(log_files) == 0:
        raise ValueError('No (valid) log files found in path: ' + video_path)
    out_lines = list()
    for segment_no, log_file in log_files:
        out_lines += generate_segment_lines(segment_no, log_file)
    with open(video_path + OUT_DAT_FILE, 'w') as f:
        f.writelines(out_lines)

