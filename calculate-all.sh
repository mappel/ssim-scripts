#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

./calculate-segment-ssims-frames.sh "$LOG_DIR"
./calculate-ssim-thresholds-frames.sh "$LOG_DIR"
./translate-segment-ssims-to-percent.sh "$LOG_DIR"
./translate-thresholds-to-percent.sh "$LOG_DIR"
./generate-3d-data.sh "$LOG_DIR"
./calculate-stats.sh "$LOG_DIR"
