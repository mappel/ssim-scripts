#!/usr/bin/python3
import sys
import os


ACC_FRAME_SIZES_FILE = 'acc-frame-sizes.dat'
THRESHOLDS_FRAME_FILE = 'thresholds-frames.dat'
THRESHOLDS_PERCENT_FILE = 'thresholds-percent.dat'


def get_acc_frame_sizes_for_segment(segment_no: int, video_path: str):
    sizes_file = video_path + str(segment_no) + '/' + ACC_FRAME_SIZES_FILE
    ret = list()
    with open(sizes_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                raise ValueError('Invalid frame size line: ' + line + '\n in file: ' + sizes_file)
            ret.append(int(line_split[1]))
    return ret


def translate_thresholds(thresholds: list, frame_sizes: list):
    max_size = frame_sizes[0]
    ret = list()
    for threshold in thresholds:
        absolute_size = frame_sizes[int(threshold)]
        # For consistency we output the percentage we can discard.
        relative_size = 100 - ((absolute_size / max_size) * 100)
        ret.append(relative_size)
    return ret


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/video>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    input_file = video_path + THRESHOLDS_FRAME_FILE
    output_lines = list()
    with open(input_file, 'r') as f:
        header_row = f.readline()
        output_lines.append(header_row)
        threshold_column_count = len(header_row.split()) - 1
        if threshold_column_count <= 0:
            raise ValueError('No threshold columns found in file: ' + input_file)
        for line in f:
            line_split = line.split()
            if len(line_split) != threshold_column_count + 1:
                raise ValueError('Invalid threshold line: ' + line + '\n in file: ' + input_file)
            segment_no = int(line_split[0])
            segment_frame_sizes = get_acc_frame_sizes_for_segment(segment_no, video_path)
            percent_thresholds = translate_thresholds(line_split[1:threshold_column_count + 1], segment_frame_sizes)
            output_lines.append(str(segment_no) + ' ' + ' '.join(map(str, percent_thresholds)) + '\n')
    with open(video_path + THRESHOLDS_PERCENT_FILE, 'w') as f:
        f.writelines(output_lines)

