set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 21 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics ls 81, ls 81, ls 82

set xtics border in scale 0.5,0.25 nomirror autojustify
set ytics border in scale 0.5,0.25 nomirror autojustify

set style line 1 lc rgb "#b2e2e2"
set style line 2 lc rgb "#66c2a4"
set style line 3 lc rgb "#2ca25f"
set style line 4 lc rgb "#006d2c"
set pm3d at bs
set palette defined (0 "#3f007d", 0.5 "#de2d26", 1 "#2ca25f")
set ticslevel 0
set view 60,15

set xrange [0:76]
set xlabel "Segments" offset 0,-1

set yrange[96:0]
set ytics 20 font ',16' offset -0.5,0
set ylabel "Dropped frames" rotate parallel offset 1,-1

set zrange[0:1]
set ztics offset 0.5,0
set zlabel "SSIM score" rotate offset 2,0

set cbrange[0:1]
set cbtics font ',16' offset -0.75,0

unset key

set output OUT_FILE
splot IN_FILE u 1:3:2 w pm3d
unset output
