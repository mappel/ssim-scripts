set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 0.5,0.25 nomirror norotate autojustify
set ytics border in scale 0.5,0.25 nomirror norotate autojustify

set style line 1 lc rgb "#b2e2e2"
set style line 2 lc rgb "#66c2a4"
set style line 3 lc rgb "#2ca25f"
set style line 4 lc rgb "#006d2c"

set style line 4 lc rgb "#1b9e77"
set style line 3 lc rgb "#d95f02" 
set style line 2 lc rgb "#7570b3"
set style line 1 lc rgb "#2c7bb6" 

set xrange [0:76]
set xtics 10 offset 0,0.5
set mxtics 2
set xlabel "Segments" offset 0,1

set yrange[0:100]
set ytics 20 offset 0.5,0
set mytics 2
set ylabel "Amount kept (percent)" offset 2,0

set key autotitle columnhead
set key outside top center horizontal maxrows 1
set key samplen 1.5

set style fill solid noborder

set output OUT_FILE
plot \
    IN_FILE u 1:(100 - $2) w boxes ls 4, \
    '' u 1:(100 - $3) w boxes ls 3, \
    '' u 1:(100 - $4) w boxes ls 2, \
    '' u 1:(100 - $5) w boxes ls 1
unset output
