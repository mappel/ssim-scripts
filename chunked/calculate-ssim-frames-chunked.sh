#!/bin/bash
set -eou pipefail

if [ ! $# -eq 4 ]
then
    echo "usage: $0 <path/to/logs> <number of segments> <frames in segment> <frames in chunk>"
    exit
fi

LOG_DIR=${1%/}
SEGMENT_COUNT="$2"
SEGMENT_FRAMES="$3"
CHUNK_FRAMES="$4"

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    ./calculate-ssim-frames-chunked.py "$VIDEO" "$SEGMENT_COUNT" "$SEGMENT_FRAMES" "$CHUNK_FRAMES"
done
