#!/usr/bin/python3
import sys
import re
import os

SIZES_FILE = 'acc-frame-sizes.dat'
LOG_FILE_SUFFIX = '.log'
RANGE_FILE_SUFFIX = '.range'


def write_size_files(out_path: str, acc_sizes: list):
    dropped_frame_counter = 0
    with open(out_path + SIZES_FILE, 'w') as f:
        for acc_size in acc_sizes:
            f.write(str(dropped_frame_counter) + ' ' + str(acc_size) + '\n')
            dropped_frame_counter += 1


def get_size_from_line(line: str):
    line_split = line.strip().split()
    if len(line_split) != 2:
        print('Error: Invalid line', line.strip(), file=sys.stderr)
        return None
    return int(line_split[1]) - int(line_split[0]) + 1


def get_size_from_file(range_file: str):
    if not os.path.exists(range_file):
        print('Error: Failed to find range file:', range_file, file=sys.stderr)
        return None
    acc_size = 0
    with open(range_file, 'r') as f:
        for line in f:
            size = get_size_from_line(line)
            if size is None:
                return None
            acc_size += size
    return acc_size


def process_segment(segment_log_path: str, segment_proc_path: str):
    largest_log = -1
    it = os.scandir(segment_log_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_FILE_SUFFIX):
            log_no = -1
            try:
                log_no = int(entry.name[:-len(LOG_FILE_SUFFIX)])
            except ValueError as e:
                print('Warning: Skipping log file with invalid name:', entry.name, file=sys.stderr)
                print('in folder:', segment_log_path, file=sys.stderr)
                continue
            largest_log = max(largest_log, log_no)
    segment_size = get_size_from_file(segment_proc_path + str(largest_log) + RANGE_FILE_SUFFIX)
    if segment_size is None:
        return
    remaining_sizes = [segment_size] # Zero frames dropped
    for log_idx in range(1, largest_log + 1):
        range_file = segment_proc_path + str(log_idx) + RANGE_FILE_SUFFIX
        size = get_size_from_file(range_file)
        if size is None:
            return
        remaining_sizes.append(segment_size - size)
    write_size_files(segment_log_path, remaining_sizes)


def process_video(video_log_path: str, video_proc_path: str):
    it = os.scandir(video_log_path)
    for entry in it:
        if entry.is_dir():
            segment_log_path = video_log_path + entry.name + '/'
            segment_proc_path = video_proc_path + entry.name + '/'
            if not os.path.exists(segment_proc_path):
                print('Warning: Could not find matching segment proc folder',
                        segment_proc_path, file=sys.stderr)
                return
            process_segment(segment_log_path, segment_proc_path)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/logs> <path/to/proc>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    proc_path = sys.argv[2]
    if not proc_path.endswith('/'):
        proc_path += '/'
    it = os.scandir(log_path)
    for entry in it:
        if entry.is_dir():
            video_log_path = log_path + entry.name + '/'
            video_proc_path = proc_path + entry.name + '/'
            if not os.path.exists(video_proc_path):
                print('Warning: Could not find matching proc folder',
                        video_proc_path, file=sys.stderr)
                continue
            process_video(video_log_path, video_proc_path)

