#!/usr/bin/python3
import sys
import os


ACC_FRAME_SIZES_FILE = 'acc-frame-sizes.dat'
SSIM_FRAME_FILE = 'ssim-frames.dat'
SSIM_PERCENT_FILE = 'ssim-percent.dat'
SSIM_PERCENT_FILE_NORMALIZED = 'ssim-percent-normalized.dat'


def get_acc_frame_sizes_from_file(chunk_path: str):
    sizes_file = chunk_path + ACC_FRAME_SIZES_FILE
    ret = list()
    with open(sizes_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                print('Error: Invalid frame size line: ' + line + '\n in file: ' + sizes_file, file=sys.stderr)
                print('in file: ' + sizes_file, file=sys.stderr)
                return None
            ret.append(int(line_split[1]))
    return ret


def get_max_size_from_file(acc_sizes_file: str):
    if not os.path.exists(acc_sizes_file):
        print('Error: Could not find accumulated frame sizes for chunk', acc_sizes_file, file=sys.stderr)
        return None
    with open(acc_sizes_file, 'r') as f:
        line = f.readline().strip()
        line_split = line.split()
        if len(line_split) != 2:
            print('Error: Invalid line', line.strip(), file=sys.stderr)
            return None
        if line_split[0] != '0':
            print('Warning: Expected line with format "0 ..." but got', line, '. Something is fishy.', file=sys.stderr)
        return int(line_split[1])


def process_chunk(chunk_path: str, segment_size: int):
    chunk_frame_sizes = get_acc_frame_sizes_from_file(chunk_path)
    if chunk_frame_sizes is None:
        return
    chunk_size = chunk_frame_sizes[0]
    input_file = chunk_path + SSIM_FRAME_FILE
    output_lines = list()
    output_lines_normalized = list()
    with open(input_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                raise ValueError('Invalid SSIM line: ' + line + '\n in file: ' + input_file)
            discarded_frame_count = int(line_split[0])
            absolute_size = chunk_frame_sizes[discarded_frame_count]
            relative_size = ((chunk_size - absolute_size) / chunk_size) * 100
            relative_size_normalized = ((chunk_size - absolute_size) / segment_size) * 100
            output_lines.append(str(relative_size) + ' ' + ' '.join(line_split[1:]) + '\n')
            output_lines_normalized.append(str(relative_size_normalized) + ' ' + ' '.join(line_split[1:]) + '\n')
    with open(chunk_path + SSIM_PERCENT_FILE, 'w') as f:
        f.writelines(output_lines)
    with open(chunk_path + SSIM_PERCENT_FILE_NORMALIZED, 'w') as f:
        f.writelines(output_lines_normalized)


def process_segment(segment_path: str, segment_frames: int, chunk_frames: int):
    chunk_count = int(segment_frames / chunk_frames)
    segment_size = 0
    # Gather all chunk sizes first so that we can calculate a normalized
    # percentage in the second passs.
    for chunk_no in range(1, chunk_count + 1):
        chunk_acc_sizes = segment_path + str(chunk_no) + '/' + ACC_FRAME_SIZES_FILE
        chunk_size = get_max_size_from_file(chunk_acc_sizes)
        if chunk_size is None:
            continue
        segment_size += chunk_size
    for chunk_no in range(1, chunk_count + 1):
        chunk_path = segment_path + str(chunk_no) + '/'
        if not os.path.exists(segment_path):
            print('Error: Missing chunk', chunk_no, 'in segment', segment_path, file=sys.stderr)
            continue
        process_chunk(chunk_path, segment_size)


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print('usage:', sys.argv[0], '<path/to/video> <number of segments> <frames in segment> <frames in chunk>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    segment_count = int(sys.argv[2])
    segment_frames = int(sys.argv[3])
    chunk_frames = int(sys.argv[4])
    for segment_no in range(1, segment_count + 1):
        segment_path = video_path + str(segment_no) + '/'
        if not os.path.exists(segment_path):
            print('Error: Missing segment', segment_no, file=sys.stderr)
            continue
        process_segment(segment_path, segment_frames, chunk_frames)

