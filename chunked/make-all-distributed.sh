#!/bin/bash
set -eou pipefail
if [ ! $# -eq 3 ]
then
    echo "usage: $0 <path/to/logs> <path/to/proc> <number of segments>"
    exit
fi

LOG_PATH=${1%/}
PROC_PATH=${2%/}
SEG_COUNT="$3"

./calculate-accumulated-frame-sizes-distributed.py "$LOG_PATH" "$PROC_PATH"
./calculate-ssim-frames-distributed.sh "$LOG_PATH"
./translate-ssim-to-percent-distributed.sh "$LOG_PATH" "$SEG_COUNT"

