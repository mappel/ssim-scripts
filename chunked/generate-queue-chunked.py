#!/usr/bin/python3
import sys
import re
import subprocess as sp
import os
from collections import namedtuple

class Fixture:
    def __init__(self):
        self.kill_input = str()
        self.ref_input = str()
        self.init_segment = str()
        self.kill_range = str()
        self.log_output = str()
        self.scratch_path = str()


PROCESS_SCRIPT = '/pool/data/mappel/ssim-labels/process.sh'
QUEUE_FILE = 'queue.parallel'
BASEURL_PREFIX = '.*dash'

FrameWeight = namedtuple('FrameWeight', 'weight frame')

def find_reference_file(name: str, reference_path: str):
    reference_file = sp.check_output(['find', reference_path, '-name', name]).strip().decode('utf-8')
    if reference_file == '':
        raise ValueError('Failed to find file "' + name + '" in path: ' + reference_path)
    if len(reference_file.split()) > 1:
        raise ValueError('Multiple file locations found for file: ' + name)
    return reference_file


def write_range_file(path: str, ranges: list):
    with open(path, 'w') as f:
        for start, end in ranges:
            f.write(str(start) + ' ' + str(end) + '\n')


def make_sorted_frameweight_list(frames: list, weights: list, chunk_idx: int):
    temp = list()
    if len(frames) != len(weights):
        print('Error: Sizes of frame and weight lists do not match.', file=sys.stderr)
        return ret
    for idx, frame in enumerate(frames):
        # Do not sort I-frame
        if chunk_idx == 0 and idx == 0:
            continue
        temp.append((weights[idx], frame))
    temp.sort(reverse=True)
    ret = list()
    if chunk_idx == 0:
        # I-frame is most important
        ret.append(frames[0])
    for weight, frame in temp:
        ret.append(frame)
    return ret


def generate_per_chunk_fixtures(segment_ranges: dict, segment_weights: dict, prefix: str, init_segment: str, reference_path: str, scratch_path: str, output_path: str, kill_input_path: str, chunk_frames: int):
    fixtures = list()
    for segment_no, ranges in sorted(segment_ranges.items()):
        weights = None
        if segment_weights is not None:
            weights = segment_weights[segment_no]
            if len(ranges) != len(weights):
                print('Error: Number of frame ranges and weights does not match for segment', segment_no, 'in video', prefix, file=sys.stderr)
                continue
        segment_prefix = str(segment_no) + '_' + prefix
        segment_file = find_reference_file(segment_prefix + '.mp4', kill_input_path)
        reference_segment_file = find_reference_file(segment_prefix + '_glued.mp4', reference_path)
        segment_scratch_folder = scratch_path + str(segment_no) + '/'
        segment_log_folder = output_path + str(segment_no) + '/'
        chunk_idx = 0
        while chunk_idx * chunk_frames < len(ranges):
            chunk_scratch_folder = segment_scratch_folder + str(chunk_idx + 1) + '/'
            os.makedirs(chunk_scratch_folder, exist_ok=True)
            chunk_log_folder = segment_log_folder + str(chunk_idx + 1) + '/'
            start_idx = chunk_idx * chunk_frames
            end_idx = start_idx + chunk_frames - 1
            sorted_frames = ranges[start_idx:end_idx + 1]
            if weights is not None:
                sorted_frames = make_sorted_frameweight_list(ranges[start_idx:end_idx + 1], weights[start_idx:end_idx + 1], chunk_idx)
            iteration_start = 0
            if chunk_idx == 0:
                # Do not remove the I-frame
                iteration_start = 1
            for i in range(iteration_start, len(sorted_frames) + 1):
                removed_frame_count = len(sorted_frames) - i
                range_file = chunk_scratch_folder + str(removed_frame_count) + '.range'
                log_file = chunk_log_folder + str(removed_frame_count) + '.log'
                write_range_file(range_file, sorted_frames[i:])
                fixture = Fixture()
                fixture.kill_input = segment_file
                fixture.ref_input = reference_segment_file
                fixture.init_segment = init_segment
                fixture.kill_range = range_file
                fixture.log_output = log_file
                fixture.scratch_path = chunk_scratch_folder
                fixtures.append(fixture)
            chunk_idx += 1
    return fixtures

def generate_chunk_distributed_fixtures(segment_ranges: dict, segment_weights: dict, prefix: str, init_segment: str, reference_path: str, scratch_path: str, output_path: str, kill_input_path: str, chunk_frames: int):
    fixtures = list()
    for segment_no, ranges in sorted(segment_ranges.items()):
        weights = None
        if segment_weights is not None:
            weights = segment_weights[segment_no]
            if len(ranges) != len(weights):
                print('Error: Number of frame ranges and weights does not match for segment', segment_no, 'in video', prefix, file=sys.stderr)
                continue
        segment_prefix = str(segment_no) + '_' + prefix
        segment_file = find_reference_file(segment_prefix + '.mp4', kill_input_path)
        reference_segment_file = find_reference_file(segment_prefix + '_glued.mp4', reference_path)
        segment_scratch_folder = scratch_path + str(segment_no) + '/'
        os.makedirs(segment_scratch_folder, exist_ok=True)
        segment_log_folder = output_path + str(segment_no) + '/'
        chunk_idx = 0
        sorted_chunk_frames = list()
        while chunk_idx * chunk_frames < len(ranges):
            start_idx = chunk_idx * chunk_frames
            end_idx = start_idx + chunk_frames - 1
            sorted_frames = ranges[start_idx:end_idx + 1]
            if weights is not None:
                sorted_frames = make_sorted_frameweight_list(ranges[start_idx:end_idx + 1], weights[start_idx:end_idx + 1], chunk_idx)
            sorted_chunk_frames.append(sorted_frames)
            chunk_idx += 1
        for i in range(0, len(sorted_chunk_frames[0]) + 1):
            removed_frame_count = len(sorted_chunk_frames[0]) - i
            range_file = segment_scratch_folder + str(removed_frame_count) + '.range'
            log_file = segment_log_folder + str(removed_frame_count) + '.log'
            delete_range = list()
            for idx, chunk_frame_list in enumerate(sorted_chunk_frames):
                # Do not drop I-Frame
                if idx == 0 and i == 0:
                    delete_range += chunk_frame_list[1:]
                else:
                    delete_range += chunk_frame_list[i:]
            write_range_file(range_file, delete_range)
            fixture = Fixture()
            fixture.kill_input = segment_file
            fixture.ref_input = reference_segment_file
            fixture.init_segment = init_segment
            fixture.kill_range = range_file
            fixture.log_output = log_file
            fixture.scratch_path = segment_scratch_folder
            fixtures.append(fixture)
    return fixtures


def write_fixtures_to_queue(scratch_path: str, fixtures: list):
    queue_file = scratch_path + QUEUE_FILE
    with open(queue_file, 'w') as f:
        for fixture in fixtures:
            line = [PROCESS_SCRIPT]
            line.append(fixture.kill_input)
            line.append(fixture.ref_input)
            line.append(fixture.init_segment)
            line.append(fixture.kill_range)
            line.append(fixture.log_output)
            line.append(fixture.scratch_path)
            f.write(' '.join(line) + '\n')

def get_ranges_from_file(range_file: str):
    ranges = list()
    segment_ranges = dict()
    segment_offsets = dict()
    sidx_count = 1
    segment_count = 0
    with open(range_file, 'r') as f:
        # Consume header.
        f.readline()
        for line in f:
            line_split = line.strip().split(',')
            if len(line_split) != 4:
                print('Error: Invalid line:', line, file=sys.stderr)
                continue
            line_type = line_split[1]
            start = int(line_split[2])
            end = int(line_split[3])
            if line_type == 'sidx':
                segment_offsets[sidx_count] = start
                sidx_count += 1
            elif 'content' in line_type:
                if line_type == 'I_content':
                    if segment_count > 0:
                        segment_ranges[segment_count] = list(ranges)
                        ranges.clear()
                    segment_count += 1
                ranges.append((start - segment_offsets[segment_count], end - segment_offsets[segment_count]))
    if len(ranges) > 0:
        segment_ranges[segment_count] = list(ranges)
    return segment_ranges


def get_weights_from_file(weight_file: str):
    pairs = list()
    with open(weight_file, 'r') as f:
        for line in f:
            line_split = line.strip().split()
            if len(line_split) != 2:
                print('Error: Invalid line:', line.strip(), file=sys.stderr)
                continue
            # List contains (frame number, weight)
            pairs.append((int(line_split[0]), int(line_split[1])))
    pairs.sort()
    ret = list()
    for frame_num, weight in pairs:
        ret.append(weight)
    return ret


def get_weights(weight_path: str):
    segment_weights = dict()
    it = os.scandir(weight_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('.dat'):
            file_name = os.path.splitext(entry.name)[0]
            file_name_split = file_name.split('-')
            if len(file_name_split) != 2:
                print('Warning: Failed to get segment number from file:', entry.name, file=sys.stderr)
                continue
            segment_no = int(file_name_split[1])
            weights = get_weights_from_file(weight_path + entry.name)
            segment_weights[segment_no] = weights
    return segment_weights


def parse_ranges(reference_path: str, scratch_path: str, output_path: str, kill_input_path: str, range_path: str, weight_path_suffix: str, chunk_frames: int):
    fixtures = list()
    curr_prefix = ''
    curr_init_segment = ''
    curr_scratch_path = ''
    curr_output_path = ''
    segment_count = 1
    it = os.scandir(range_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('_dashinit-ranges.csv'):
            curr_prefix = entry.name.rstrip('init-ranges.csv')
            init_segment_name = '0_' + curr_prefix + '.mp4'
            segment_count = 1
            curr_init_segment = find_reference_file(init_segment_name, reference_path)
            curr_scratch_path = scratch_path + curr_prefix + '/'
            curr_output_path = output_path + curr_prefix + '/'
            segment_ranges = get_ranges_from_file(range_path + entry.name)
            segment_weights = None
            if weight_path_suffix != '':
                segment_weights = get_weights(range_path + entry.name[:-len('_dashinit-ranges.csv')] + '-' + weight_path_suffix + '/')
            fixtures += generate_per_chunk_fixtures(segment_ranges, segment_weights, curr_prefix, curr_init_segment, reference_path, curr_scratch_path, curr_output_path, kill_input_path, chunk_frames)
            #fixtures += generate_chunk_distributed_fixtures(segment_ranges, segment_weights, curr_prefix, curr_init_segment, reference_path, curr_scratch_path, curr_output_path, kill_input_path, chunk_frames)
    write_fixtures_to_queue(scratch_path, fixtures)


if __name__  == "__main__":
    if len(sys.argv) < 7:
        print('usage: ' + sys.argv[0] + ' <path/to/reference> <path/to/kill-input> <path/to/scratch> <path/to/output> <path/to/ranges> <frames in chunk> [<weight path suffix>]')
        exit(1)
    reference_path = sys.argv[1]
    if not reference_path.endswith('/'):
        reference_path += '/'
    kill_input_path = sys.argv[2]
    if not kill_input_path.endswith('/'):
        kill_input_path += '/'
    scratch_path = sys.argv[3]
    if not scratch_path.endswith('/'):
        scratch_path += '/'
    output_path = sys.argv[4]
    if not output_path.endswith('/'):
        output_path += '/'
    range_path = sys.argv[5]
    if not range_path.endswith('/'):
        range_path += '/'
    chunk_frames = int(sys.argv[6])
    weight_path_suffix = ''
    if len(sys.argv) == 8:
        weight_path_suffix = sys.argv[7]
    parse_ranges(reference_path, scratch_path, output_path, kill_input_path, range_path, weight_path_suffix, chunk_frames)

