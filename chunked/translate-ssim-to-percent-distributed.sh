#!/bin/bash
set -eou pipefail

if [ ! $# -eq 2 ]
then
    echo "usage: $0 <path/to/logs> <number of segments>"
    exit
fi

LOG_DIR=${1%/}
SEGMENT_COUNT="$2"

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    ./translate-ssim-to-percent-distributed.py "$VIDEO" "$SEGMENT_COUNT"
done
