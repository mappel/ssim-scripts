#!/usr/bin/python3
import sys
import os


OUTFILE_NAME = 'ssim-frames.dat'
LOG_FILE_SUFFIX = '.log'


def parse_line(line: str):
    line_split = line.split()
    if len(line_split) != 6:
        raise ValueError('Invalid SSIM line: ' + line)
    y = float(line_split[1].lstrip('Y:'))
    u = float(line_split[2].lstrip('U:'))
    v = float(line_split[3].lstrip('V:'))
    all = float(line_split[4].lstrip('All:'))
    return y,u,v,all


def parse_logfile(log_file: str, segment_frames: int):
    y_acc = 0
    u_acc = 0
    v_acc = 0
    all_acc = 0
    line_count = 0
    with open(log_file, 'r') as f:
        for line in f:
            y,u,v,all = parse_line(line)
            y_acc += y
            u_acc += u
            v_acc += v
            all_acc += all
            line_count += 1
    if line_count != segment_frames:
        print('Error: Log file does not meet expected frame count. Expected',
                segment_frames, 'got', line_count, file=sys.stderr)
        print('in file: ' + log_file, file=sys.stderr)
        return None
    y_avg = y_acc / line_count
    u_avg = u_acc / line_count
    v_avg = v_acc / line_count
    all_avg = all_acc / line_count
    return y_avg, u_avg, v_avg, all_avg


def write_ssim_file(path: str, scores: list):
    with open(path + OUTFILE_NAME, 'w') as f:
        for log_no, y, u, v, all in scores:
            line = [str(log_no)]
            line.append(str(y))
            line.append(str(u))
            line.append(str(v))
            line.append(str(all))
            f.write(' '.join(line) + '\n')


def process_chunk(chunk_path: str, chunk_no: int, segment_frames: int, chunk_frames: int):
    ssims = list()
    log_count = chunk_frames
    if chunk_no == 1:
        # First chunk does not drop I-frame
        log_count = chunk_frames - 1
    for log_no in range(log_count + 1):
        log_file = chunk_path + str(log_no) + LOG_FILE_SUFFIX
        if not os.path.exists(log_file):
            print('Error: Missing log file', log_file, file=sys.stderr)
            return
        ssim_scores = parse_logfile(log_file, segment_frames)
        if ssim_scores is None:
            return
        ssims.append((log_no, *ssim_scores))
    write_ssim_file(chunk_path, ssims)


def process_segment(segment_path: str, segment_frames: int, chunk_frames: int):
    chunk_count = int(segment_frames / chunk_frames)
    for chunk_no in range(1, chunk_count + 1):
        chunk_path = segment_path + str(chunk_no) + '/'
        if not os.path.exists(chunk_path):
            print('Error: Missing chunk', chunk_no, 'in segment', segment_path,
                    file=sys.stderr)
            continue
        process_chunk(chunk_path, chunk_no, segment_frames, chunk_frames)


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print('usage: ' + sys.argv[0] + ' <path/to/video> <number of segments> <frames in segment> <frames in chunk>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    segment_count = int(sys.argv[2])
    segment_frames = int(sys.argv[3])
    chunk_frames = int(sys.argv[4])
    for segment_no in range(1, segment_count + 1):
        segment_path = log_path + str(segment_no) + '/'
        if not os.path.exists(segment_path):
            print('Error: Missing segment', segment_no, file=sys.stderr)
            continue
        process_segment(segment_path, segment_frames, chunk_frames)
    exit(0)

