#!/usr/bin/python3
import sys
import os


ACC_FRAME_SIZES_FILE = 'acc-frame-sizes.dat'
SSIM_FRAME_FILE = 'ssim-frames.dat'
SSIM_PERCENT_FILE = 'ssim-percent.dat'


def get_acc_frame_sizes_from_file(chunk_path: str):
    sizes_file = chunk_path + ACC_FRAME_SIZES_FILE
    if not os.path.exists(sizes_file):
        print('Error: Could not find accumulated frame sizes for segment', acc_sizes_file, file=sys.stderr)
        return None
    ret = list()
    with open(sizes_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                print('Error: Invalid frame size line: ' + line + '\n in file: ' + sizes_file, file=sys.stderr)
                print('in file: ' + sizes_file, file=sys.stderr)
                return None
            ret.append(int(line_split[1]))
    return ret


def process_segment(segment_path: str):
    segment_frame_sizes = get_acc_frame_sizes_from_file(segment_path)
    if segment_frame_sizes is None:
        return
    segment_size = segment_frame_sizes[0]
    input_file = segment_path + SSIM_FRAME_FILE
    if not os.path.exists(input_file):
        print('Error: Could not find ssim frame file for segment', input_file, file=sys.stderr)
        return
    output_lines = list()
    with open(input_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 5:
                raise ValueError('Invalid SSIM line: ' + line + '\n in file: ' + input_file)
            discarded_frame_count = int(line_split[0])
            absolute_size = segment_frame_sizes[discarded_frame_count]
            relative_size = ((segment_size - absolute_size) / segment_size) * 100
            output_lines.append(str(relative_size) + ' ' + ' '.join(line_split[1:]) + '\n')
    with open(segment_path + SSIM_PERCENT_FILE, 'w') as f:
        f.writelines(output_lines)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], '<path/to/video> <number of segments>')
        exit(1)
    video_path = sys.argv[1]
    if not video_path.endswith('/'):
        video_path += '/'
    segment_count = int(sys.argv[2])
    for segment_no in range(1, segment_count + 1):
        segment_path = video_path + str(segment_no) + '/'
        if not os.path.exists(segment_path):
            print('Error: Missing segment', segment_no, file=sys.stderr)
            continue
        process_segment(segment_path)

