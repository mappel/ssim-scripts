#!/usr/bin/python3
import sys
import re
import os

SIZES_FILE = 'acc-frame-sizes.dat'
LOG_FILE_SUFFIX = '.log'
RANGE_FILE_SUFFIX = '.range'


def write_size_files(out_path: str, acc_sizes: list):
    dropped_frame_counter = 0
    with open(out_path + SIZES_FILE, 'w') as f:
        for acc_size in acc_sizes:
            f.write(str(dropped_frame_counter) + ' ' + str(acc_size) + '\n')
            dropped_frame_counter += 1


def get_size_from_line(line: str):
    line_split = line.strip().split()
    if len(line_split) != 2:
        print('Error: Invalid line', line.strip(), file=sys.stderr)
        return None
    return int(line_split[1]) - int(line_split[0]) + 1


def get_accumulated_sizes(range_file: str):
    # The ranges are ordered from dropped last to dropped first, i.e., the last
    # range in the file is the first frame that is dropped.
    # Our output format maps the number of dropped frames to the _remaining_
    # size, i.e., the remaining size for zero dropped frames is the size of the
    # chunk.
    # So to make processing easier, we return the sizes already in this order.
    ranges = list()
    with open(range_file, 'r') as f:
        ranges = f.readlines()
    # All frames dropped = 0 remaining bytes
    acc_sizes = [0]
    acc_size = 0
    for line in ranges:
        # The first line is the size of the last frame that we drop, i.e, this
        # is the remaining size if we drop all frames but one. If we start
        # accumulating from here, we get a list which we can reverse in the end
        # to get the desired format.
        size = get_size_from_line(line)
        if size is None:
            print('in file:', range_file, file=sys.stderr)
            return None
        acc_size += size
        acc_sizes.append(acc_size)
    acc_sizes.reverse()
    return acc_sizes


def process_chunk(chunk_log_path: str, chunk_proc_path: str):
    largest_log_no = -1
    it = os.scandir(chunk_log_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_FILE_SUFFIX):
            log_no = int(entry.name[:-len(LOG_FILE_SUFFIX)])
            largest_log_no = max(largest_log_no, log_no)
    range_file = chunk_proc_path + str(largest_log_no) + RANGE_FILE_SUFFIX
    if not os.path.exists(range_file):
        print('Warning: Could not find matching range file',
                range_file, file=sys.stderr)
        return
    acc_sizes = get_accumulated_sizes(range_file)
    if acc_sizes is None:
        return
    write_size_files(chunk_log_path, acc_sizes)


def process_segment(segment_log_path: str, segment_proc_path: str):
    it = os.scandir(segment_log_path)
    for entry in it:
        if entry.is_dir():
            chunk_log_path = segment_log_path + entry.name + '/'
            chunk_proc_path = segment_proc_path + entry.name + '/'
            if not os.path.exists(chunk_proc_path):
                print('Warning: Could not find matching chunk proc folder',
                        chunk_proc_path, file=sys.stderr)
                return
            process_chunk(chunk_log_path, chunk_proc_path)


def process_video(video_log_path: str, video_proc_path: str):
    it = os.scandir(video_log_path)
    for entry in it:
        if entry.is_dir():
            segment_log_path = video_log_path + entry.name + '/'
            segment_proc_path = video_proc_path + entry.name + '/'
            if not os.path.exists(segment_proc_path):
                print('Warning: Could not find matching segment proc folder',
                        segment_proc_path, file=sys.stderr)
                return
            process_segment(segment_log_path, segment_proc_path)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/logs> <path/to/proc>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    proc_path = sys.argv[2]
    if not proc_path.endswith('/'):
        proc_path += '/'
    it = os.scandir(log_path)
    for entry in it:
        if entry.is_dir():
            video_log_path = log_path + entry.name + '/'
            video_proc_path = proc_path + entry.name + '/'
            if not os.path.exists(video_proc_path):
                print('Warning: Could not find matching proc folder',
                        video_proc_path, file=sys.stderr)
                continue
            process_video(video_log_path, video_proc_path)

