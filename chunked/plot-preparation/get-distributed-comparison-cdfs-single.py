#!/usr/bin/python3
import numpy as np
import os
import sys
from collections import namedtuple


SSIM_PERCENT_FILE = 'ssim-percent.dat'

SSIMLine = namedtuple('SSIMLine', 'percent y u v all')


def parse_ssim_line(line: str):
    line_split = line.strip().split()
    if len(line_split) != 5:
        print('Error: Invalid SSIM line:', line.strip(), file=sys.stderr)
        return None
    percent = float(line_split[0])
    y = float(line_split[1])
    u = float(line_split[2])
    v = float(line_split[3])
    score_all = float(line_split[4])
    return SSIMLine(percent, y, u, v, score_all)


def parse_ssim_file(ssim_file: str):
    ssim_lines = list()
    with open(ssim_file, 'r') as f:
        for line in f:
            parsed_line = parse_ssim_line(line)
            if parsed_line is None:
                return None
            ssim_lines.append(parsed_line)
    return ssim_lines


def process_segment(distributed_segment_ssim_file: str, no_chunks_segment_ssim_file: str):
    distributed_ssim_lines = parse_ssim_file(distributed_segment_ssim_file)
    no_chunks_ssim_lines = parse_ssim_file(no_chunks_segment_ssim_file)
    if distributed_ssim_lines is None or no_chunks_ssim_lines is None:
        return None
    distributed_ssims = list()
    no_chunks_ssims = list()
    for distributed_line in distributed_ssim_lines:
        distributed_ssims.append(distributed_line.all)
        found = False
        for idx, no_chunks_line in enumerate(no_chunks_ssim_lines):
            if distributed_line.percent < no_chunks_line.percent:
                if idx == 0:
                    no_chunks_ssims.append(no_chunks_line.all)
                else:
                    no_chunks_ssims.append(no_chunks_ssim_lines[idx - 1].all)
                found = True
                break
        if not found:
            no_chunks_ssims.append(no_chunks_ssim_lines[-1].all)
    if len(distributed_ssims) != len(no_chunks_ssims):
        print('Error: Number of SSIMs does not match. (',
                len(distributed_ssims), '!=', len(no_chunks_ssims),
                file=sys.stderr)
        return None
    return distributed_ssims, no_chunks_ssims


def process_video(distributed_video_path: str, no_chunks_video_path: str, segment_count: int):
    distributed_ssims = list()
    no_chunks_ssims = list()
    for segment_no in range(1, segment_count + 1):
        distributed_segment_ssim_file = distributed_video_path \
        + str(segment_no) + '/' + SSIM_PERCENT_FILE
        no_chunks_segment_ssim_file = no_chunks_video_path + str(segment_no) \
        + '/' + SSIM_PERCENT_FILE
        if not os.path.exists(distributed_segment_ssim_file) \
        or not os.path.exists(no_chunks_segment_ssim_file):
            print('Error: Missing segment SSIM file:', file=sys.stderr)
            print(distributed_segment_ssim_file, file=sys.stderr)
            print('or', file=sys.stderr)
            print(no_chunks_segment_ssim_file, file=sys.stderr)
            print('Aborting this video.', file=sys.stderr)
            return None
            return None
        ssims = process_segment(distributed_segment_ssim_file, no_chunks_segment_ssim_file)
        if ssims is None:
            return None
        distributed_ssims += ssims[0]
        no_chunks_ssims += ssims[1]
    ref_len = len(distributed_ssims)
    if len(no_chunks_ssims) != ref_len:
        print('Error: Length of SSIM lists does not match. (', ref_len, '!=',
                len(no_chunks_ssims), ')', file=sys.stderr)
        return None
    distributed_ssims.sort()
    no_chunks_ssims.sort()
    p_values = np.arange(ref_len) / (ref_len - 1)
    cdf_lines = ['p distributed no_chunks\n']
    for idx in range(ref_len):
        line = [p_values[idx], distributed_ssims[idx], no_chunks_ssims[idx]]
        cdf_lines.append(' '.join(map(str, line)) + '\n')
    return cdf_lines


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('usage:', sys.argv[0], '<path/to/distributed-logs> <path/to/no-chunks-logs> <path/to/output> <number of segments>')
        exit(1)
    distributed_log_path = sys.argv[1]
    if not distributed_log_path.endswith('/'):
        distributed_log_path += '/'
    no_chunks_log_path = sys.argv[2]
    if not no_chunks_log_path.endswith('/'):
        no_chunks_log_path += '/'
    output_path = sys.argv[3]
    if not output_path.endswith('/'):
        output_path += '/'
    if not os.path.exists(output_path):
        print('Error: Could not find output path:', output_path, file=sys.stderr)
        exit(1)
    segment_count = int(sys.argv[4])
    cdf_data = dict()
    it = os.scandir(distributed_log_path)
    for entry in it:
        if entry.is_dir():
            distributed_video_path = distributed_log_path + entry.name + '/'
            no_chunks_video_path = no_chunks_log_path + entry.name + '/'
            if not os.path.exists(no_chunks_log_path):
                print('Error: Could not find matching video path:', no_chunks_video_path, file=sys.stderr)
                continue
            video_cdfs = process_video(distributed_video_path, no_chunks_video_path, segment_count)
            if video_cdfs is None:
                continue
            cdf_data[entry.name] = video_cdfs
    for video in cdf_data:
        output_name = video + '-distributed-no-chunks-ssim-comp-cdf.dat'
        output_file = output_path + output_name
        with open(output_file, 'w') as f:
            f.writelines(cdf_data[video])

