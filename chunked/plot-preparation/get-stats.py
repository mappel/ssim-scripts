#!/usr/bin/python3
import numpy as np
import os
import sys

NUM_DROPPED_FRAMES = 12
NUM_DISTRIBUTIONS = 2   # distributed, no chunks
NUM_SETS = 2            # original, reorder
DAT_SUFFIX = '.dat'
INPUT_PREFIX_END = '_dash'

def get_input_files(input_path: str):
    ret = dict()
    it = os.scandir(input_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(DAT_SUFFIX):
            ret[entry.name] = input_path + entry.name
    return ret


def get_input_prefix(input_file: str):
    prefix_pos = input_file.find(INPUT_PREFIX_END)
    if prefix_pos < 0:
        print('Error: Could not match input prefix "' + INPUT_PREFIX_END + '" to file:', input_file, file=sys.stderr)
        return None
    return input_file[:prefix_pos + len(INPUT_PREFIX_END)]


def parse_line(line: str, value_list: list, set_idx: int, distribution_idx):
    # +1 for 'no frames dropped' column
    columns_per_set = (NUM_DROPPED_FRAMES + 1) * NUM_DISTRIBUTIONS
    # +1 for p column
    expected_col_num = 1 + columns_per_set * NUM_SETS
    line_split = line.split()
    if len(line_split) != expected_col_num:
        print('Error: Invalid line:', line, file=sys.stderr)
        return False
    range_start = 1 + set_idx * columns_per_set
    range_end = range_start + columns_per_set
    value_idx = 0
    for idx in range(range_start, range_end, NUM_DISTRIBUTIONS):
        value_list[value_idx].append(float(line_split[idx + distribution_idx]))
        value_idx += 1
    return True


def process_file(input_file: str):
    original_values = [[] for i in range(NUM_DROPPED_FRAMES + 1)]
    reorder_values = [[] for i in range(NUM_DROPPED_FRAMES + 1)]
    with open(input_file, 'r') as f:
        # Consume headers
        f.readline()
        for line in f:
            if not parse_line(line.strip(), original_values, 0, 0):
                return None
            if not parse_line(line.strip(), reorder_values, 1, 0):
                return None
    original_values_arr = np.array(original_values)
    original_max_values = np.max(original_values_arr, axis=1)
    original_min_values = np.min(original_values_arr, axis=1)
    original_median_values = np.median(original_values_arr, axis=1)
    original_q1_values, original_q3_values = np.percentile(original_values_arr,[25, 75], axis=1)

    reorder_values_arr = np.array(reorder_values)
    reorder_max_values = np.max(reorder_values_arr, axis=1)
    reorder_min_values = np.min(reorder_values_arr, axis=1)
    reorder_median_values = np.median(reorder_values_arr, axis=1)
    reorder_q1_values, reorder_q3_values = np.percentile(reorder_values_arr,[25, 75], axis=1)
    ret_lines = ['order dropped_frames min max median q1 q3\n']
    for idx in range(0, NUM_DROPPED_FRAMES + 1):
        line = ['original']
        line.append(idx)
        line.append(original_min_values[idx])
        line.append(original_max_values[idx])
        line.append(original_median_values[idx])
        line.append(original_q1_values[idx])
        line.append(original_q3_values[idx])
        ret_lines.append(' '.join(map(str, line)) + '\n')

        line = ['reorder']
        line.append(idx)
        line.append(reorder_min_values[idx])
        line.append(reorder_max_values[idx])
        line.append(reorder_median_values[idx])
        line.append(reorder_q1_values[idx])
        line.append(reorder_q3_values[idx])
        ret_lines.append(' '.join(map(str, line)) + '\n')
    return ret_lines


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], '<path/to/input> <path/to/output>')
        exit(1)
    input_path = sys.argv[1]
    if not input_path.endswith('/'):
        input_path += '/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    input_files = get_input_files(input_path)
    for input_file in input_files:
        print(input_file)
        prefix = get_input_prefix(input_file)
        if prefix is None:
            continue
        lines = process_file(input_files[input_file])
        if lines is None:
            continue
        with open(output_path + prefix + '-distributed-comparison-original-chain-weights-1p.dat', 'w') as f:
            f.writelines(lines)

