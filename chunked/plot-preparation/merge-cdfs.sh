#!/bin/bash
if [ ! $# -eq 3 ]
then
    echo "usage: $0 <path/to/input-1> <path/to/input-2> <path/to/output>"
    exit
fi

INPUT_1=${1%/}
INPUT_2=${2%/}
OUTPUT=${3%/}

for VIDEO_1 in "$INPUT_1"/*.dat
do
    VIDEO_NAME=$(basename $VIDEO_1)
    VIDEO_2="$INPUT_2"/$VIDEO_NAME
    if [ ! -f "$VIDEO_2" ]
    then
        echo "Error: Could not find matching file $VIDEO_2"
        continue
    fi
    join -j 1 "$VIDEO_1" "$VIDEO_2" >> "$OUTPUT"/"$VIDEO_NAME"
done
