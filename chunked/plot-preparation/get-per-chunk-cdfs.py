#!/usr/bin/python3
import numpy as np
import os
import sys
from collections import namedtuple


SSIM_PERCENT_FILE = 'ssim-percent.dat'

SSIMLine = namedtuple('SSIMLine', 'percent y u v all')


def parse_ssim_line(line: str):
    line_split = line.strip().split()
    if len(line_split) != 5:
        print('Error: Invalid SSIM line:', line.strip(), file=sys.stderr)
        return None
    percent = float(line_split[0])
    y = float(line_split[1])
    u = float(line_split[2])
    v = float(line_split[3])
    score_all = float(line_split[4])
    return SSIMLine(percent, y, u, v, score_all)


def get_percentage_from_file(ssim_file: str, threshold: float):
    ssim_lines = list()
    with open(ssim_file, 'r') as f:
        for line in f:
            parsed_line = parse_ssim_line(line)
            if parsed_line is None:
                return None
            ssim_lines.append(parsed_line)
    for idx, ssim_line in enumerate(ssim_lines):
        if ssim_line.all < threshold:
            if idx == 0:
                return ssim_line.percent
            else:
                return ssim_lines[idx - 1].percent
    # Last entry is still over threshold.
    return ssim_lines[-1].percent


def process_segment(segment_path: str, chunk_count: int, threshold: float):
    per_chunk_percentage = list()
    for chunk_no in range(1, chunk_count + 1):
        chunk_ssim_file = segment_path + str(chunk_no) + '/' + SSIM_PERCENT_FILE
        if not os.path.exists(chunk_ssim_file):
            print('Error: Failed to find SSIM file:', chunk_ssim_file, file=sys.stderr)
            return None
        percent = get_percentage_from_file(chunk_ssim_file, threshold)
        if percent is None:
            return None
        per_chunk_percentage.append(percent)
    return per_chunk_percentage


def process_video(video_path: str, segment_count: int, chunk_count: int, threshold: float):
    chunk_percentages = [[] for i in range(chunk_count)]
    for segment_no in range(1, segment_count + 1):
        segment_path = video_path + str(segment_no) + '/'
        if not os.path.exists(segment_path):
            print('Error: Missing segment folder:', segment_path, file=sys.stderr)
            print('Aborting this video.', file=sys.stderr)
            return None
        per_chunk_percentage = process_segment(segment_path, chunk_count, threshold)
        if per_chunk_percentage is None:
            return None
        if len(per_chunk_percentage) != chunk_count:
            print('Error: Got an invalid number of chunk percentages. Expected', chunk_count, 'got', len(process_segment), file=sys.stderr)
            return None
        for idx, percent in enumerate(per_chunk_percentage):
            chunk_percentages[idx].append(percent)
    ref_len = len(chunk_percentages[0])
    # Sanity check and CDF preparation
    for chunk in chunk_percentages:
        if len(chunk) != ref_len:
            print('Error: Length of chunk lists does not match. (', ref_len, '!=', len(chunk), ')', file=sys.stderr)
            return None
        chunk.sort()
    p_values = np.arange(ref_len) / (ref_len - 1)
    cdf_lines = ['p ' + ' '.join(map(str, range(1, chunk_count + 1))) + '\n']
    for idx in range(ref_len):
        line = [p_values[idx]]
        for chunk in chunk_percentages:
            line.append(chunk[idx])
        cdf_lines.append(' '.join(map(str, line)) + '\n')
    return cdf_lines


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print('usage:', sys.argv[0], '<path/to/logs> <path/to/output> <number of segments> <number of chunks> <threshold>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    if not os.path.exists(output_path):
        print('Error: Could not find output path:', output_path, file=sys.stderr)
        exit(1)
    segment_count = int(sys.argv[3])
    chunk_count = int(sys.argv[4])
    threshold = float(sys.argv[5])
    cdf_data = dict()
    it = os.scandir(log_path)
    for entry in it:
        if entry.is_dir():
            video_path = log_path + entry.name + '/'
            video_cdfs = process_video(video_path, segment_count, chunk_count, threshold)
            if video_cdfs is None:
                continue
            cdf_data[entry.name] = video_cdfs
    for video in cdf_data:
        output_name = video + '-' + str(threshold) + '-dropped-percent-cdf.dat'
        output_file = output_path + output_name
        with open(output_file, 'w') as f:
            f.writelines(cdf_data[video])

