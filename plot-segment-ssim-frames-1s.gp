set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 0.5,0.25 nomirror norotate autojustify
set ytics border in scale 0.5,0.25 nomirror norotate autojustify

set style line 1 lc rgb "#2a71b0"

unset key

set xrange [0:24]
set xtics 10 offset 0,0.5
set mxtics 2
set xlabel "Dropped frames" offset 0,1

set ylabel "SSIM score" offset 1,0

set output OUT_FILE
stats IN_FILE u 1:5 nooutput
set yrange[STATS_min_y - 0.1:1]
plot IN_FILE u 1:5 w steps ls 1
unset output
