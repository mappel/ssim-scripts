#!/usr/bin/python3
import sys
import re
import subprocess as sp
import os

class Fixture:
    def __init__(self):
        self.kill_input = str()
        self.ref_input = str()
        self.init_segment = str()
        self.kill_range = str()
        self.log_output = str()
        self.scratch_path = str()


PROCESS_SCRIPT = '/pool/data/mappel/ssim-labels/process.sh'
QUEUE_FILE = 'queue.parallel'
BASEURL_PREFIX = '.*dash'

def get_prefix_from_baseurl(line: str):
    tag_match = re.compile('>.*<')
    prefix_match = re.compile(BASEURL_PREFIX)
    tag_result = tag_match.search(line)
    if tag_result is None:
        raise ValueError('Failed to match BaseURL tags in line: ' + line)
    baseurl = tag_result.group()[1:-1]
    print('Found BaseURL: ' + baseurl)
    prefix_result = prefix_match.match(baseurl)
    if prefix_result is None:
        raise ValueError('Failed to match BaseURL prefix "' + BASEURL_PREFIX + '" in BaseURL: ' + baseurl)
    prefix = prefix_result.group()
    return prefix

def find_reference_file(name: str, reference_path: str):
    reference_file = sp.check_output(['find', reference_path, '-name', name]).strip().decode('utf-8')
    if reference_file == '':
        raise ValueError('Failed to find file "' + name + '" in path: ' + reference_path)
    if len(reference_file.split()) > 1:
        raise ValueError('Multiple file locations found for file: ' + name)
    return reference_file


def get_ranges_from_segmenturl(line: str):
    ranges = list()
    line = line.strip()
    line_split = line.split()
    frames_pos = -1
    media_range_pos = -1
    for idx, field in enumerate(line_split):
        if field.startswith('unreliable'):
            frames_pos = idx
        elif field.startswith('mediaRange'):
            media_range_pos = idx
    if frames_pos == -1 or media_range_pos == -1:
        raise ValueError('Failed to find frame list or mediaRange in SegmentURL: ' + line)
    media_range = line_split[media_range_pos]
    media_range = media_range.lstrip('mediaRange="').rstrip('"/>')
    media_range_split = media_range.split('-')
    if len(media_range_split) != 2:
        raise ValueError('Invalid mediaRange attribute: ' + media_range)
    offset = int(media_range_split[0])
    frame_list = line_split[frames_pos]
    frame_list = frame_list.lstrip('unreliable="').rstrip('"/>')
    frames = frame_list.split(',')
    for frame in frames:
        range_split = frame.split('-')
        if len(range_split) != 2:
            raise ValueError('Invalid byte range: ' + frame)
        start = int(range_split[0]) - offset
        end = int(range_split[1]) - offset
        ranges.append((start, end))
    return ranges


def write_range_file(path: str, ranges: list):
    with open(path, 'w') as f:
        for start, end in ranges:
            f.write(str(start) + ' ' + str(end) + '\n')

def generate_fixtures(segment_count: int, prefix: str, init_segment: str, reference_path: str, scratch_path: str, output_path: str, frame_ranges: list, kill_input_path: str):
    fixtures = list()
    segment_prefix = str(segment_count) + '_' + prefix
    segment_file = find_reference_file(segment_prefix + '.mp4', kill_input_path)
    reference_segment_file = find_reference_file(segment_prefix + '_glued.mp4', reference_path)
    scratch_folder = scratch_path + str(segment_count) + '/'
    log_folder = output_path + str(segment_count) + '/'
    os.makedirs(scratch_folder, exist_ok=True)
    for i in range(0, len(frame_ranges) + 1):
        removed_frame_count = len(frame_ranges) - i
        range_file = scratch_folder + str(removed_frame_count) + '.range'
        log_file = log_folder + str(removed_frame_count) + '.log'
        write_range_file(range_file, frame_ranges[i:])
        fixture = Fixture()
        fixture.kill_input = segment_file
        fixture.ref_input = reference_segment_file
        fixture.init_segment = init_segment
        fixture.kill_range = range_file
        fixture.log_output = log_file
        fixture.scratch_path = scratch_folder
        fixtures.append(fixture)
    return fixtures


def write_fixtures_to_queue(scratch_path: str, fixtures: list):
    queue_file = scratch_path + QUEUE_FILE
    with open(queue_file, 'w') as f:
        for fixture in fixtures:
            line = [PROCESS_SCRIPT]
            line.append(fixture.kill_input)
            line.append(fixture.ref_input)
            line.append(fixture.init_segment)
            line.append(fixture.kill_range)
            line.append(fixture.log_output)
            line.append(fixture.scratch_path)
            f.write(' '.join(line) + '\n')


def parse_mpd(mpd_file: str, reference_path: str, scratch_path: str, output_path: str, kill_input_path: str):
    fixtures = list()
    curr_prefix = ''
    curr_init_segment = ''
    curr_scratch_path = ''
    curr_output_path = ''
    segment_count = 1
    with open(mpd_file, 'r') as f:
        for line in f:
            if 'BaseURL' in line:
                curr_prefix = get_prefix_from_baseurl(line)
                init_segment_name = '0_' + curr_prefix + '.mp4'
                segment_count = 1
                curr_init_segment = find_reference_file(init_segment_name, reference_path)
                curr_scratch_path = scratch_path + curr_prefix + '/'
                curr_output_path = output_path + curr_prefix + '/'
            elif 'SegmentURL' in line:
                frame_ranges = get_ranges_from_segmenturl(line)
                fixtures += generate_fixtures(segment_count, curr_prefix, curr_init_segment, reference_path, curr_scratch_path, curr_output_path, frame_ranges, kill_input_path)
                segment_count += 1
    write_fixtures_to_queue(scratch_path, fixtures)


if __name__  == "__main__":
    if not len(sys.argv) == 6:
        print('usage: ' + sys.argv[0] + ' <reference-path> <kill-input-path> <scratch-path> <output-path> <MPD>')
        exit(1)
    reference_path = sys.argv[1]
    if not reference_path.endswith('/'):
        reference_path += '/'
    kill_input_path = sys.argv[2]
    if not kill_input_path.endswith('/'):
        kill_input_path += '/'
    scratch_path = sys.argv[3]
    if not scratch_path.endswith('/'):
        scratch_path += '/'
    output_path = sys.argv[4]
    if not output_path.endswith('/'):
        output_path += '/'
    mpd_file = sys.argv[5]
    parse_mpd(mpd_file, reference_path, scratch_path, output_path, kill_input_path)

