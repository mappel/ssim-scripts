#!/bin/bash
set -eou pipefail
DAT_FILE_NAME=ssim-percent.dat
OUT_FILE_NAME=ssim-percent-all.pdf
PLOT_SCRIPT=plot-segment-ssim-percent-all.gp

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    VIDEO=${VIDEO%/}
    SEGMENTS=$(find $VIDEO -type d | wc -l)
    SEGMENTS=$((SEGMENTS -1))
    OUT_FILE="$VIDEO"/"$OUT_FILE_NAME"
    gnuplot -e 'PREFIX="'"$VIDEO"'"' -e 'SEGMENTS='"$SEGMENTS" -e 'OUT_FILE="'"$OUT_FILE"'"' -e 'DAT_FILE="'"$DAT_FILE_NAME"'"' "$PLOT_SCRIPT"
done
