#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

./calculate-all.sh "$LOG_DIR"
./plot-all.sh "$LOG_DIR"
