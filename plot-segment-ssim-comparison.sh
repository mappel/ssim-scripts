#!/bin/bash
set -eou pipefail
OUT_FILE_NAME=ssim-segment-comparison.pdf
PLOT_SCRIPT=plot-segment-ssim-comparison.gp

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    VIDEO=${VIDEO%/}
    SEGMENTS=$(find $VIDEO -maxdepth 1 -name *.dat | wc -l)
    OUT_FILE="$VIDEO"/"$OUT_FILE_NAME"
    gnuplot -e 'PREFIX="'"$VIDEO"'"' -e 'SEGMENTS='"$SEGMENTS" -e 'OUT_FILE="'"$OUT_FILE"'"' "$PLOT_SCRIPT"
done
