#!/usr/bin/python3
import sys
import os


ACC_FRAME_SIZES_FILE = 'acc-frame-sizes.dat'
PSNR_FRAME_FILE = 'psnr-frames.dat'
PSNR_PERCENT_FILE = 'psnr-percent.dat'


def get_acc_frame_sizes_for_segment(segment_path: str):
    sizes_file = segment_path + ACC_FRAME_SIZES_FILE
    ret = list()
    with open(sizes_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                raise ValueError('Invalid frame size line: ' + line + '\n in file: ' + sizes_file)
            ret.append(int(line_split[1]))
    return ret


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/segment>')
        exit(1)
    segment_path = sys.argv[1]
    if not segment_path.endswith('/'):
        segment_path += '/'
    segment_frame_sizes = get_acc_frame_sizes_for_segment(segment_path)
    max_size = segment_frame_sizes[0]
    input_file = segment_path + PSNR_FRAME_FILE
    output_lines = list()
    with open(input_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 2:
                raise ValueError('Invalid PSNR line: ' + line + '\n in file: ' + input_file)
            discarded_frame_count = int(line_split[0])
            absolute_size = segment_frame_sizes[discarded_frame_count]
            relative_size = 100 - ((absolute_size / max_size) * 100)
            output_lines.append(str(relative_size) + ' ' + line_split[1] + '\n')
    with open(segment_path + PSNR_PERCENT_FILE, 'w') as f:
        f.writelines(output_lines)

