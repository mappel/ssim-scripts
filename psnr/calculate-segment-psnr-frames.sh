#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}

for VIDEO in "$LOG_DIR"/*/
do
    echo "$VIDEO"
    for SEGMENT in "$VIDEO"*/
    do
        ./calculate-segment-psnr-frames.py "$SEGMENT"
    done
done
