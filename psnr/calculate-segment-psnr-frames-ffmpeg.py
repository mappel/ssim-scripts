#!/usr/bin/python3
import sys
import os


OUTFILE_NAME = 'psnr-frames.dat'
LOG_FILE_SUFFIX = '.log'
EXPECTED_FRAME_COUNT = 96


def get_logfiles(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_FILE_SUFFIX):
            log_no = int(entry.name[:-len(LOG_FILE_SUFFIX)])
            ret.append((log_no, path + entry.name))
    ret.sort()
    return ret


def parse_line(line: str):
    line_split = line.split()
    if len(line_split) != 9:
        raise ValueError('Invalid PSNR line: ' + line)
    frame = int(line_split[0].split(':')[1])
    # Choose psnr_y like VMAF does
    value = line_split[6].split(':')[1]
    if value == 'inf':
        # Map inf to 60 like VMAF does
        return frame, 60.0
    else:
        return frame, float(value)


def parse_logfile(file: str):
    highest_frame_found = 0
    score_acc = 0
    with open(file, 'r') as f:
        for line in f:
            frame, value = parse_line(line.strip())
            highest_frame_found = frame
            score_acc += value
    if highest_frame_found != EXPECTED_FRAME_COUNT:
        print('Warning: Log file does not meet expected frame count. Expected ' + str(EXPECTED_FRAME_COUNT) + ' got '
                + str(highest_frame_found), file=sys.stderr)
        print('in file: ' + file, file=sys.stderr)
    score_acc /= highest_frame_found
    return score_acc


def write_psnr_file(path: str, scores: list):
    with open(path + OUTFILE_NAME, 'w') as f:
        for log_no, score in scores:
            line = [str(log_no), str(score)]
            f.write(' '.join(line) + '\n')


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/segment>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    log_files = get_logfiles(log_path)
    if len(log_files) == 0:
        print('Error: No log files found in path: ' + log_path, file=sys.stderr)
        exit(1)
    scores = list()
    for log_no, log_file in log_files:
        score = parse_logfile(log_file)
        scores.append((log_no, score))
    write_psnr_file(log_path, scores)
    exit(0)
