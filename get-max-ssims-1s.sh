#!/bin/bash
set -eou pipefail

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/logs>"
    exit
fi

LOG_DIR=${1%/}
readonly LOG_DIR
readonly EXPECTED_SEGMENT_COUNT=300

for VIDEO in "$LOG_DIR"/*/
do
    ./get-max-ssims.py "$VIDEO" "$EXPECTED_SEGMENT_COUNT"
done
