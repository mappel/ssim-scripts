#!/usr/bin/python3
import sys
import re
import os

BASEURL_PREFIX = '.*dash'
SIZES_FILE = 'acc-frame-sizes.dat'


def get_prefix_from_baseurl(line: str):
    tag_match = re.compile('>.*<')
    prefix_match = re.compile(BASEURL_PREFIX)
    tag_result = tag_match.search(line)
    if tag_result is None:
        raise ValueError('Failed to match BaseURL tags in line: ' + line)
    baseurl = tag_result.group()[1:-1]
    print('Found BaseURL: ' + baseurl)
    prefix_result = prefix_match.match(baseurl)
    if prefix_result is None:
        raise ValueError('Failed to match BaseURL prefix "' + BASEURL_PREFIX + '" in BaseURL: ' + baseurl)
    prefix = prefix_result.group()
    return prefix

def get_frame_sizes_from_segmenturl(line: str):
    sizes = list()
    line = line.strip()
    line_split = line.split()
    frames_pos = -1
    for idx, field in enumerate(line_split):
        if field.startswith('unreliable'):
            frames_pos = idx
            break
    if frames_pos == -1:
        raise ValueError('Failed to find frame list in SegmentURL: ' + line)
    frame_list = line_split[frames_pos]
    frame_list = frame_list.lstrip('unreliable="').rstrip('"/>')
    frames = frame_list.split(',')
    for frame in frames:
        range_split = frame.split('-')
        if len(range_split) != 2:
            raise ValueError('Invalid byte range: ' + frame)
        start = int(range_split[0])
        end = int(range_split[1])
        sizes.append(end - start + 1)
    return sizes


def accumulate_frames(frame_sizes: list):
    acc = 0
    ret = [0]
    for size in frame_sizes:
        acc += size
        ret.append(acc)
    # We use '#dropped frames' as the index everywhere so be consistent.
    ret.reverse()
    return ret


def parse_mpd(mpd_file: str):
    ret = list()
    curr_prefix = ''
    segment_count = 1
    with open(mpd_file, 'r') as f:
        for line in f:
            if 'BaseURL' in line:
                curr_prefix = get_prefix_from_baseurl(line)
                segment_count = 1
            elif 'SegmentURL' in line:
                frame_sizes = get_frame_sizes_from_segmenturl(line)
                acc_frame_sizes = accumulate_frames(frame_sizes)
                out_suffix = curr_prefix + '/' + str(segment_count) + '/'
                ret.append((out_suffix, acc_frame_sizes))
                segment_count += 1
    return ret


def write_size_files(out_path: str, out_list: list):
    for path, entries in out_list:
        output_dir = out_path + path
        if not os.path.exists(output_dir):
            continue
        dropped_frame_counter = 0
        with open(output_dir + SIZES_FILE, 'w') as f:
            for entry in entries:
                f.write(str(dropped_frame_counter) + ' ' + str(entry) + '\n')
                dropped_frame_counter += 1


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/logs> <MPD>')
        exit(1)
    log_path = sys.argv[1]
    if not log_path.endswith('/'):
        log_path += '/'
    mpd_file = sys.argv[2]
    out_list = parse_mpd(mpd_file)
    write_size_files(log_path, out_list)

